// Copyright bartuka3 FEE CUT (c) 2023
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
// Optimised for standard 120-len lines
#define DEFAULT_LINE_STRING_CHAR_SIZE 40
// Always alloc one char more for the null byte
#define STRING_REALLOC_FACTOR(X) (((X) << 1) + 1)

#define COLOR_START "\x1B[01;31m\x1B[K"
#define COLOR_END "\x1B[m\x1B[K"

enum flags { flags_none = 0, flags_e = 1, flags_color = 2, flags_stdin = 4 };
enum quantifiers { q_any = '*', q_one_or_more = '+', q_zero_or_one = '.' };

const static char *FLAG_E = "-E";
const static char *FLAG_COLOR = "--color=always";

// REUSABLE FUNCTIONS
int error(int code);
size_t strLen(char *str);
int _compareString(char *str1, char *str2);
int _attempt_string_realloc(char **string, size_t *current_str_length,
                            size_t new_size);
int _getLine(char **curr_line, size_t *curr_line_length, FILE **file_stream);
// char *strCopy(char *src);
void printStringSegment(char *line, int start, int stop);

int error(int code) {
  switch (code) {
  case 101:
    fprintf(stderr, "Cannot read the requested file.\n");
    return -1;
  case 102:
    fprintf(stderr, "Not enough parameters.\n");
    return -1;
  }
  return code;
}

// STRING FUNCTIONS

size_t strLen(char *str) {
  // O(n) string length computation
  size_t index = 0;
  while (str[index] != '\0') {
    index++;
  }
  return index;
}
int _compareString(char *str1, char *str2) {
  // O(n) string length comparison
  int index = 0;
  while (str1[index] != '\0' && str2[index] != '\0') {
    if (str1[index] != str2[index]) {
      return 0;
    }
    index++;
  }
  return 1;
}
int _attempt_string_realloc(char **string, size_t *current_str_length,
                            size_t new_size) {
  // Attempts to resize *string using the realloc scaling factor
  char *tmp_string =
      realloc(*string, (*current_str_length + new_size) * sizeof(char));
  if (tmp_string == NULL) {
    // We tripped and fell on realloc
    return -1;
  }
  // now we can safely write into the curr_line pointer
  *string = tmp_string;
  *current_str_length = *current_str_length + new_size;
  return 0;
}

int _getLine(char **curr_line, size_t *curr_line_length, FILE **file_stream) {
  /* Writes a line from *file_stream into *curr_line, saves the allocated length
   * to curr_line_length to optimise the number of reallocs */

  // if we are passed a NULL line we need to create one
  if (*curr_line == NULL) {
    *curr_line = calloc(DEFAULT_LINE_STRING_CHAR_SIZE, sizeof(char));
    if (*curr_line == NULL) {
      // The malloc had to fail
      return -1;
    }
    *curr_line_length = DEFAULT_LINE_STRING_CHAR_SIZE;
  }

  // Now we can finally start writing into the string
  char buffer; // we don't initialise since we have a guaranteed write 2 lines
  // after and writing \0 here might confuse the author later
  int write_position = 0; // where we are in line_buffer
  do {
    buffer = fgetc(*file_stream);
    if (write_position >= *curr_line_length) {
      // we need to realloc curr_line
      if (_attempt_string_realloc(curr_line, curr_line_length,
                                  STRING_REALLOC_FACTOR(*curr_line_length))) {
        // here one comes to die from a realloc error
        return -1;
      };
    }

    // now it's safe to push the buffer into the string
    (*curr_line)[write_position++] = buffer;
  } while (buffer != EOF && buffer != '\n');

  // Here we sacrifice a single byte per line to a higher order to save us
  // from reading uninitialised memory.
  if (_attempt_string_realloc(curr_line, curr_line_length, 1)) {
    // here one comes to die from a realloc error for the second time
    return -1;
  }
  (*curr_line)[write_position++] = '\0';
  // We throw on EOF to stop further line queries
  if (buffer == EOF) {
    return -2;
  }
  return write_position;
}
/*
char *strCopy(char *src) {
  // Returns a char* copy of a src C-string
  size_t strL = strLen(src);
  char *dst = malloc(strL + 1);
  for (int char_i = 0; char_i < strL; char_i++) {
    dst[char_i] = src[char_i];
  }
  return dst;
}
*/

void printStringSegment(char *line, int start, int stop) {
  // O(n) string print
  size_t index = start;
  // We need to ensure we aren't printing null bytes
  while (index <= stop && line[index] != '\0') {
    putchar(line[index]);
    index++;
  }
}

void printStrSegmentWithColor(char *line, int pattern_len, int start,
                              int stop) {
  // prints a string segment
  // we already assume that color_pos+pattern_len < strlen(line)

  size_t index = start;
  // we need to ensure we aren't printing null bytes as well
  while (index <= stop && line[index] != '\0') {
    if (index == stop - pattern_len + 1) {
      printf(COLOR_START); // start formatting
    }
    putchar(line[index]);
    // we are converting length to index, so we subtract 1
    index++;
  }
  // end formatting
  printf(COLOR_END);
}

// GREP

int grepLine(char *line, char *pattern, size_t line_size, size_t pattern_len,
             bool print_color) {
  int matching = 0;
  int print_start = -1; // last index we printed from
  int recent_match = -1;
  for (int i = 0; i < line_size; i++) {
    if (line[i] == pattern[matching]) {
      matching++;
    } else if (matching) {
      matching = 0;
      i--;
    }
    if (matching == (int)pattern_len) {
      // we hit and thus we print
      print_start = (print_start < 0 ? 0 : print_start);
      recent_match = i - pattern_len + 1;
      if (print_color) {
        printStrSegmentWithColor(line, pattern_len, print_start, i);
      } else {
        printStringSegment(line, print_start, i);
      }
      print_start = i + 1; // next time we have to print from i+1
    }
  }
  if (recent_match >= 0) {
    // we have leftove buffer to print from the line
    printStringSegment(line, print_start, line_size - 1);
  }
  return print_start;
}

// HW04-opt
int grepLineRegex(char *line, char *pattern, size_t line_size,
                  size_t pattern_len) {
  /* WIP

  int matching = 0;
  char regex_state = '\0';
  int times_regex_used = 0;
  int regex_start = 0;
  for (int i = 0; i < line_size; i++) {
    if (line[i] == pattern[matching]) {
      matching++;
    } else if (matching) {
      regex_state = pattern[matching];
      switch (regex_state) {
      case q_any:
      case q_zero_or_one:
      case q_one_or_more:
        if (pattern[matching - 1] == line[i]) {
          if (!regex_start) {
            regex_start = i;
          }
        }
      }
      if (regex_state == q_any || regex_state == q_one_or_more ||
          regex_state == q_zero_or_one) {
        if (pattern[matching - 1] == line[i]) {
          if (!regex_start) {
            regex_start = i;
          }
          times_regex_used++;
        } else if (!times_regex_used) {
          matching = 0;
          i = regex_start;
        }
      }

    wrong_match:
      matching = 0;
      i--;
    }
  match_done_check:
    if (matching == (int)pattern_len) {
      return i - pattern_len + 1;
    }
  }*/
  return -2;
}

int grepFile(FILE *file, char *pattern, int mode_flags) {
  int matched_lines = 0;
  int current_match_pos; // needs to be an int since negative values are errors
  char *line_buffer = NULL;
  size_t line_buffer_len = 0;
  size_t pattern_length = strLen(pattern);

  int line_length = _getLine(&line_buffer, &line_buffer_len, &file);
  while (line_length >= 0) {
    // First we check if we need to do regex and perform the check
    if (mode_flags & flags_e) {
      current_match_pos =
          grepLineRegex(line_buffer, pattern, line_length, pattern_length);
    } else {
      current_match_pos = grepLine(line_buffer, pattern, line_length,
                                   pattern_length, mode_flags & flags_color);
    }
    // Now we see if we got a valid match <==> current_match_pos is in [0,inf)
    if (current_match_pos >= 0) {
      matched_lines++;
    }
    // And finally we ask for another line
    line_length = _getLine(&line_buffer, &line_buffer_len, &file);
  }
  // We are done with the line buffer, let's be nice and free it
  free(line_buffer);
  return matched_lines;
}

// SPECIFIC

/* Reads parameters and attempts to fill all the input pointers
 * returns 0 if everything goes well, otherwise a code is thrown
 * */
int processInput(int argc, char *argv[], FILE **file, char **pattern,
                 int *mode_flags) {
  int arg_num = 0;
  while (++arg_num < argc) {

    if (_compareString(argv[arg_num], (char *)FLAG_E)) {
      *mode_flags |= flags_e;
      continue;
    } else if (_compareString(argv[arg_num], (char *)FLAG_COLOR)) {
      *mode_flags |= flags_color;
      continue;
    } else if (*pattern != NULL) { // FILE LOADING
      *file = fopen(argv[arg_num], "r");
      if (*file == NULL) { // load fail
        return error(101);
      }
    } else {
      // PATTERN LOADING
      *pattern = argv[arg_num];
    }
  }
  if (*pattern == NULL) {
    return error(102);
  }
  if (*file == NULL) { // if nothing is in input we are using stdin instead
    *file = stdin;
    *mode_flags |= flags_stdin;
  }
  return 0;
}

int main(int argc, char *argv[]) {
  int return_value = 0;
  int mode_flags = flags_none;
  char *pattern = NULL;
  FILE *file = NULL;

  // INPUT START
  int input_status = processInput(argc, argv, &file, &pattern, &mode_flags);
  if (input_status) {
    goto free_and_leave;
  }
  // INPUT DONE

  // The entirety of the grepping
  int lines_changed = grepFile(file, pattern, mode_flags);
  if (!lines_changed) {
    return_value = 1;
  }
free_and_leave:
  // now we can free the file but not stdin, we don't need to free stdin
  if (file != NULL && !(mode_flags &= flags_stdin)) {

    fclose(file);
  }

  return return_value;
}