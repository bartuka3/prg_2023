// Copyright bartuka3 FEE CUT (c) 2023
#ifndef GREP_H
#define GREP_H

#include <stddef.h>
#include <stdio.h>

int error(int code);

size_t strLen(char *str);
int _compareString(char *str1, char *str2);
// char *strCopy(char *src);
void printString(char *line);
void printStringWithColor(char *line, int color_pos, int pattern_len);

int _attempt_string_realloc(char **string, size_t *current_str_length);
int _getLine(char **curr_line, size_t *curr_line_length, FILE **file_stream);

int grepFile(FILE *file, char *pattern, int mode_flags);
int grepLine(char *line, char *pattern, size_t line_size, size_t pattern_len);
int grepLineRegex(char *line, char *pattern, size_t line_size,
                  size_t pattern_len);

int processInput(int argc, char *argv[], FILE **file, char **pattern,
                 int *mode_flags);
int main(int argc, char *argv[]);
#endif