// Copyright bartuka3 FEE CUT (c) 2023
/* bSML, broken Small Matrix Library */
#include "matrix.h"

#define NDEBUG

void printMatrix(matrix printed_matrix) {
  // Prints out a matrix in the specified format for HW05/man and HW05/opt.
  printf("%d %d\n", printed_matrix.rows, printed_matrix.columns);
  for (int r = 0; r < printed_matrix.rows; r++) {
    for (int c = 0; c < printed_matrix.columns; c++) {
      if (c) {
        putchar(' ');
      }
      printf("%d", printed_matrix.values[r][c]);
    }
    putchar('\n');
  }
}

matrix generateEmptyMatrix(int rows, int columns, int *err) {
  matrix tmp_matrix;
  tmp_matrix.rows = rows;
  tmp_matrix.columns = columns;
  tmp_matrix.values =
      (int **)_verifiedCalloc((size_t)tmp_matrix.rows, sizeof(int *), err);
  for (int col_index = 0; col_index < tmp_matrix.rows; col_index++) {

    tmp_matrix.values[col_index] =
        (int *)_verifiedCalloc((size_t)tmp_matrix.columns, sizeof(int), err);
  }
  return tmp_matrix;
}

matrix loadMatrixFromStdIn(int *err) {
  // Loads a matrix from standard input in the specified format for HW05/man.

#ifndef NDEBUG
  printf("\x1B[01;31m\x1B[KMATRIX REQUESTED\x1B[m\x1B[K\n");
#endif
  matrix tmp_matrix;
  tmp_matrix.rows = _loadIntFromStdIn(err);
  tmp_matrix.columns = _loadIntFromStdIn(err);
  if (*err) {
    // size load fail
    return tmp_matrix;
  }
  // Initialise space for our row pointers
  tmp_matrix.values =
      (int **)_verifiedCalloc((size_t)tmp_matrix.rows, sizeof(int *), err);

  for (int curr_row = 0; curr_row < tmp_matrix.rows; curr_row++) {
    // Before doing anything else, allocate the row
    tmp_matrix.values[curr_row] =
        (int *)_verifiedCalloc((size_t)tmp_matrix.columns, sizeof(int), err);
    // and now we can finally write into its columns
    for (int curr_col = 0; curr_col < tmp_matrix.columns; curr_col++) {
      tmp_matrix.values[curr_row][curr_col] = _loadIntFromStdIn(err);
      if (*err) {
        // matrix load fail
        // set the real row_size to where we ended
        tmp_matrix.rows = curr_row + 1;
        return tmp_matrix;
      }
    }
  }
  return tmp_matrix;
}

void destroyMatrix(matrix matrix_to_destroy) {
  // Frees all the dynamically allocated memory in the given matrix.
  if (matrix_to_destroy.values != NULL) {
    for (int r = 0; r < matrix_to_destroy.rows; r++) {
      free(matrix_to_destroy.values[r]);
    }
    free(matrix_to_destroy.values);
  }
  matrix_to_destroy.rows = DESTROYED_MARK;
  matrix_to_destroy.columns = DESTROYED_MARK;
}

void destroyMatrixBuffer(matrix *buffer, int size) {
  for (int j = 0; j < size; j++) {
    if (buffer[j].rows != DESTROYED_MARK ||
        buffer[j].columns != DESTROYED_MARK) {
      destroyMatrix(buffer[j]);
    }
  }
}

// MATRIX COMPUTATIONS
matrix addTwoMatrices(matrix first, matrix second, int *err) {
  return _addMatrixWithScalarFactor(first, second, 1, err);
}
matrix subTwoMatrices(matrix first, matrix second, int *err) {
  return _addMatrixWithScalarFactor(first, second, -1, err);
}

matrix multiplyTwoMatrices(matrix first, matrix second, int *err) {
  // Performs a very unoptimised matrix multiplication.
  matrix tmp_matrix = generateEmptyMatrix(first.rows, second.columns, err);
  if (!matrixMultiplicationSizeRequirements(first, second)) {
    *err = MATH_ERROR; // we write to the shared error state
    return tmp_matrix;
  }

  // Now we perform the iterative matrix multiplication algorithm
  // Single-letter variables are used for mathematical parity
  int sum;
  for (int i = 0; i < first.rows; i++) {
    for (int j = 0; j < second.columns; j++) {
      sum = 0;
      for (int k = 0; k < first.columns; k++) {
        sum += first.values[i][k] * second.values[k][j];
      }
      tmp_matrix.values[i][j] = sum;
    }
  }

  return tmp_matrix;
}

bool matrixAdditionSizeRequirements(matrix first, matrix second) {
  return ((first.columns == second.columns) && (first.rows == second.rows));
}
bool matrixMultiplicationSizeRequirements(matrix first, matrix second) {
  return first.columns == second.rows;
}

matrix _addMatrixWithScalarFactor(matrix first, matrix second,
                                  int second_scalar, int *err) {
  // Adds two matrices together. The second_scalar integer is applied to every
  // member of the second matrix while adding. This "hack" is here to prevent us
  // from duplicating code

  // first.rows == second.rows, my preference is to use first.rows
  matrix tmp_m = generateEmptyMatrix(first.rows, first.columns, err);
  if (!matrixAdditionSizeRequirements(first, second)) {
    *err = MATH_ERROR; // we write to the shared state
    return tmp_m;      // always return a matrix
  }

  for (int r = 0; r < first.rows; r++) {      // current row
    for (int c = 0; c < first.columns; c++) { // current column
      tmp_m.values[r][c] =
          first.values[r][c] + second_scalar * second.values[r][c];
    }
  }
  return tmp_m;
}

// INTERNAL
int _loadIntFromStdIn(int *err) {
  // Attempts to get an integer from standard input, writes an error if invalid.
  int value = -1;
  int tmp = scanf("%d", &value);
  if (tmp != 1) {
    *err = INPUT_ERROR;
    return INPUT_ERROR;
  }
  return value;
}

void *_verifiedCalloc(size_t num_of_members, size_t size, int *err) {
  // Checks the validity of a calloc, writes an error if invalid.
  void *ptr = calloc(num_of_members, size);
  if (ptr == NULL) {
    error(MEM_ERROR);
    *err = MEM_ERROR;
  }
  return ptr;
}
