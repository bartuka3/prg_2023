// Copyright bartuka3 FEE CUT 2023
#include "matrix.h"
#include <stdbool.h>
#include <stdio.h>

#ifndef NDEBUG
#define NDEBUG
#endif

enum { plus_sign = '+', minus_sign = '-', mult_sign = '*' };
const int MATRIX_BUFFER_LENGTH = 3;

// REUSABLE FUNCTIONS
char readOperatorFromStdIn();
matrix evalSimpleOperation(matrix first, matrix second, char operator,
                           int * err);
void printSituation(matrix *buff_m, char *buff_op, int number_of_matrices);

matrix evalSimpleOperation(matrix first, matrix second, char operator,
                           int * err) {
  switch (operator) {
  case plus_sign:
    return addTwoMatrices(first, second, err);
  case minus_sign:
    return subTwoMatrices(first, second, err);
  case mult_sign:
    return multiplyTwoMatrices(first, second, err);
  default:
    *err = INPUT_ERROR;
    return generateEmptyMatrix(DESTROYED_MARK, DESTROYED_MARK, err);
  }
}
int attemptFirstLoad(matrix *buf_matrix, char *buf_op, int *err) {
  // return the number of loaded matrices
  char buf = '\0';
  for (int i = 0; i < MATRIX_BUFFER_LENGTH; i++) {
    // first read, thus even the first matrix needs to be declared
    buf_matrix[i] = loadMatrixFromStdIn(err);
    if (*err) {
      // preemptively destroying the corrupted matrix
      destroyMatrix(buf_matrix[i]);
      return i - 1;
    }

    // Operator loading
    buf = readOperatorFromStdIn();
    if (buf == EOF) {
      // We hit end of file, but this is fine.
      *err = INT_END_OF_FILE;
      return i;
    } else if (buf == '\0') {
      *err = INPUT_ERROR;
      return i;
    }
    buf_op[i] = buf;
  }
  return MATRIX_BUFFER_LENGTH - 1;
}
int attemptPartialLoad(matrix *buf_matrix, char *buf_op, int start, int *err) {
  // Attempts to load further matrices, starting at start-1 in buf_matrix and
  // buf_op. Writes errors and interrupts into int* err.
  char buf = '\0';
  int index = start;
  while (index < MATRIX_BUFFER_LENGTH) {
    // now we can attempt to load a new matrix
    buf_matrix[index] = loadMatrixFromStdIn(err);
    if (*err) {
#ifndef NDEBUG
      printf("matrix not loaded\n");
#endif
      return index;
    }
    buf = readOperatorFromStdIn();
    buf_op[index] = buf;
    if (buf == EOF) {
      // We hit end of file, but this is fine.
      *err = INT_END_OF_FILE;
      return index + 1;
    } else if (buf == '\0') {
      *err = INPUT_ERROR;
      return index + 1;
    }
    index++;
  }
#ifndef NDEBUG
  printf("LOADED %d MATRICES\n", index - start);
#endif
  return index;
}

char readOperatorFromStdIn() {
  // Returns a valid operator symbol read from stdin or '\0'.
  char buff;
  int hit_newline = 0;
  do {
    buff = fgetc(stdin);
    if (buff == plus_sign || buff == minus_sign || buff == mult_sign) {
      return buff;
    } else if (buff == EOF) {
      return EOF;
    } else if (buff == '\n' && hit_newline < 1) {
      // We got an empty line, we allow one, no more.
      hit_newline += 1;
      buff = ' ';
    }
    // Only continue through whitespace
  } while (buff == ' ' || buff == '\t');
  return '\0';
}
int doProcessingRound(matrix *buf_matrix, char *buf_op,
                      int number_of_loaded_matrices, int *err) {
  /* Matrix expression evaluation
   * The scheme for the operation is as the following:
   * buf_matrix: [A      ,       B      ,      C ]
   * buf_op:     [     +-*'\0'   ,   +-*'\0'   ,   +-*'\0']
   * number_of_loaded_matrices HAS TO BE in interval [1,3]
   */

  // Index of the last existing matrix
  matrix tmp;
  int mult_register; // a register to detect multiplication in the 2nd position
  switch (number_of_loaded_matrices) {

  case 2: // A +-* B '\0'
    // We just evaluate the expression as is
    tmp = evalSimpleOperation(buf_matrix[0], buf_matrix[1], buf_op[0], err);
    // buf_matrix[0] will get sent to the shadow realm soon, let's end it now
    destroyMatrix(buf_matrix[0]);
    if (tmp.rows && tmp.columns) {
      buf_matrix[0] = tmp;
    }
    // buf_matrix[1] is obsolete as well
    destroyMatrix(buf_matrix[1]);
    // shift the operator
    buf_op[0] = buf_op[1];
    return number_of_loaded_matrices - 1; // returns 1

  case 3: // A +-* B +-*'\0' C +*-'\0'
    /* Check if we need to give precedence to the second matrix.
     * If we do, we shift the following operations by one position right.
     * Admittedly, this code is confusing *but it removes branching and
     * prefetch misses!*
     *
     * When trying to read it, it might be useful to separate
     * the branches and hardcode 'mult_register' for each branch.
     */
    mult_register = (int)(buf_op[1] == mult_sign);
    tmp = evalSimpleOperation(buf_matrix[mult_register],
                              buf_matrix[mult_register + 1],
                              buf_op[mult_register], err);
    // Send buf_matrix[mult_register] to the Shadow Realm now
    destroyMatrix(buf_matrix[mult_register]);
    buf_matrix[mult_register] = tmp;
    // Shift the operators
    buf_op[mult_register] = buf_op[mult_register + 1];
    buf_op[1] = buf_op[2];
    // buf_matrix[mult_register+1] is never winning.
    destroyMatrix(buf_matrix[mult_register + 1]);
    buf_matrix[1] = mult_register ? tmp : buf_matrix[2];
    return number_of_loaded_matrices - 1; // returns 2
  default:
    // This should NEVER occur.
    error(0xDEADBEEF);
    return 1; // If we get nonsense, we pretend we are done.
  }
}

void printSituation(matrix *buff_m, char *buff_op, int number_of_matrices) {
  /* A debug function, prints the state of the buffers. */
  printf("\033[101m");
  printf("~%d~\n", number_of_matrices);
  for (int i = 0; i < number_of_matrices; i++) {
    printMatrix(buff_m[i]);
    printf("%c\n", buff_op[i]);
  }
  printf("\033[49m");
}

int main(int argc, char *argv[]) {
  int ret_value = 0;
  bool continue_reading = true;
  matrix buffer_matrices[3];
  char buffer_operators[3] = {'\0', '\0', '\0'};
  int number_of_loaded_matrices =
      attemptFirstLoad(buffer_matrices, buffer_operators, &ret_value) + 1;
#ifndef NDEBUG
  printf("First load: %d %d\n", number_of_loaded_matrices, ret_value);
#endif
  if (ret_value) {
    // INTERRUPTS AREN'T ERRORS
    if (ret_value == INT_END_OF_FILE) {
      continue_reading = false;
      ret_value = NO_ERROR;
    } else {
      goto error_return;
    }
  }
  /* DIVIDE AND CONQUER MATRIX EXPRESSION EVALUATION
   * We take maximum of 3 matrices with their operators into a buffer,
   * evaluate for them and feed the well until we either converge or
   * Die.
   */
  while (number_of_loaded_matrices > 1 && ret_value == NO_ERROR) {
#ifndef NDEBUG
    printSituation(buffer_matrices, buffer_operators,
                   number_of_loaded_matrices);
#endif
    number_of_loaded_matrices =
        doProcessingRound(buffer_matrices, buffer_operators,
                          number_of_loaded_matrices, &ret_value);
#ifndef NDEBUG
    printSituation(buffer_matrices, buffer_operators,
                   number_of_loaded_matrices);
#endif
    if (continue_reading) {
      number_of_loaded_matrices =
          attemptPartialLoad(buffer_matrices, buffer_operators,
                             number_of_loaded_matrices, &ret_value);
      if (ret_value == INT_END_OF_FILE) {
        continue_reading = false;
        ret_value = NO_ERROR;
      }
    }
#ifndef NDEBUG
    printSituation(buffer_matrices, buffer_operators,
                   number_of_loaded_matrices);
#endif
  }
  if (!ret_value) { // we do not print anything if we catch an error
    // Finally, we can print out the resulting matrix, residing in position 0
    printMatrix(buffer_matrices[0]);
  }

error_return:
  // Let's clean the entire buffer, whatever be left inside
  destroyMatrixBuffer((matrix *)buffer_matrices, number_of_loaded_matrices);
  if (ret_value) { // Print out any error messages that might have occured
    error(ret_value);
    ret_value = INPUT_ERROR; // Assignment calls for this, so we shall deliver.
  }
  return ret_value;
}
