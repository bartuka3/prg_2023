// Copyright bartuka3 FEE CUT (c) 2023
#include <stdio.h>

// These signals are unwanted and will result in the program throwing up.
enum matrix_errors {
  NO_ERROR = 0,
  INPUT_ERROR = 100,
  MEM_ERROR = 101,
  MATH_ERROR = 103,
  DELIM_ERROR = 105,
};

// These signals aren't considered to be errors but are logged in the same way
// errors would normally be.
enum interrupts { INT_END_OF_FILE = 5 };

void error(int code);
