#include <stdbool.h>
#include <stdio.h>

#define MIN_ALLOWED_INPUT 3
#define MAX_ALLOWED_INPUT 69

int error(int code) {
  if (code == 101) {
    fprintf(stderr, "Error: Vstup mimo interval!\n");
  } else if (code == 102) {
    fprintf(stderr, "Error: Sirka neni liche cislo!\n");
  } else if (code == 103) {
    fprintf(stderr, "Error: Neplatna velikost plotu!\n");
  } else {
    fprintf(stderr, "Error: Chybny vstup!\n");
  }
  return code;
}

bool inside(int a, int min, int max) { return a >= min && a <= max; }

void repeat_n_times(char *ch, int n, bool endl) {
  // Repeats 'ch' n times
  while (n) {
    printf("%s", ch);
    n--;
  }
  if (endl) {
    printf("\n");
  }
}

void printRoof(int width) {
  int left_offset = width / 2;
  int right_offset = 0;
  while (left_offset) {
    // left roof
    printf("%*sX", left_offset, " ");
    if (right_offset) {
      // right roof - only when not at the top
      printf("%*sX", right_offset, " ");
      right_offset += 2; // each line increases the gap by 2
    } else {
      right_offset = 1; // 2nd line has always one space inside
    }
    left_offset -= 1;
    printf("\n");
  }
}

void printFenceLine(int fence_size, bool draw_line) {
  for (int fence_position = fence_size; fence_position > 0; fence_position--) {
    printf(fence_position % 2 ? "|" : (draw_line ? "-" : " "));
  }
}

int printHouseNoFence(int width, int height) {
  for (int ni = height - 2; ni > 0; ni--) { // excluding top and bottom
    printf("X");                            // left wall
    repeat_n_times(" ", width - 2, false);
    printf("X\n"); // right wall + newline
  }
  repeat_n_times("X", width, true); // floor
  return 0;
}

int printHouseFence(int width, int height, int fence_size) {
  int weight = (height - 1) % 2;             // to always start with "o"
  for (int ni = height - 2; ni > 0; ni--) {  // excluding top and bottom
    printf("X");                             // left wall
    for (int nj = width - 2; nj > 0; nj--) { // excluding left and right
      printf((nj + weight) % 2 ? "o" : "*"); // print filling
    }
    weight = !weight; // ensure diagonals
    printf("X");      // right wall
    // Fence
    if (ni == fence_size - 1) {
      printFenceLine(fence_size, true);
    } else if (ni < fence_size - 1) {
      printFenceLine(fence_size, false);
    }
    printf("\n"); // newline
  }
  repeat_n_times("X", width, false); // floor
  if (fence_size) {                  // Last fence line
    printFenceLine(fence_size, true);
  }
  printf("\n"); // newline

  return 0;
}

int main() {
  // INPUT HANDLING
  int width, height, fence_size = 0;
  bool print_fence = false;
  int status1 = scanf("%d %d", &width, &height);
  // read the two numbers
  if (status1 != 2) {
    // unsuccessful read
    return error(100);
  }
  if (width == height) {
    // read fence number
    int status2 = scanf("%d", &fence_size);
    if (status2 != 1) {
      // unsuccessful read
      return error(100);
    }
    print_fence = true;
  }
  if (!inside(width, MIN_ALLOWED_INPUT, MAX_ALLOWED_INPUT) |
      !inside(height, MIN_ALLOWED_INPUT, MAX_ALLOWED_INPUT)) {
    // Out of range
    return error(101);
  }
  if (!(width % 2)) {
    // Width not odd
    return error(102);
  }
  if (print_fence && !inside(fence_size, 1, height - 1)) {
    // fence higher than the house
    return error(103);
  }
  // DRAWING
  printRoof(width);
  repeat_n_times("X", width, true); // ceiling
  if (print_fence) {
    return printHouseFence(width, height, fence_size);
  } else {
    return printHouseNoFence(width, height);
  }
}
