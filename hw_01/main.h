// Copyright bartuka3 FEE CUT (c) 2023
#include <stdbool.h>
#define MIN_ALLOWED_INPUT 3
#define MAX_ALLOWED_INPUT 69

int error(int code);
bool inside(int a, int min, int max);
void repeat_n_times(char *ch, int n, bool endl);

void printRoof(int width);
void printFenceLine(int fence_size, bool draw_line);
int printHouseNoFence(int width, int height);
int printHouseFence(int width, int height, int fence_size);

int main();