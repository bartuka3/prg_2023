// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL.h>

#include <SDL_image.h>

#include <unistd.h> // for usleep

#ifndef SLEEP_MS
#define SLEEP_MS 1
#endif

#define THR_NUM 4
pthread_t thread_pool[THR_NUM];

#include "../lib/compute_handler.h"
#include "../lib/event_queue.h"
#include "../lib/globals.h"
#include "../lib/log.h"
#include "../lib/prg_io_nonblock.h"
#include "../lib/xwin_sdl.h"

#include <stdbool.h>

#include "../lib/keyboard_utils.h" // defines `void *keyboard_thread(void *)`
#include "../lib/pipe_thread.h"    // defines `void *pipe_thread(void *v)`
#include "../lib/render.h"         // defines `void *render_thread(void *)`

global_data_t global_data = {false, false, NULL, NULL, -1,
                             -1,    1000,  200,  false};
extern framebuffer_t GLOBAL_FRAMEBUFFER;

extern const char *INFO_STRING; // Help string for the program
// Synchronization pieces
pthread_mutex_t quit_flag_mtx;
pthread_mutex_t framebuffer_mtx;
pthread_mutex_t pipe_in_mtx;
pthread_mutex_t pipe_out_mtx;
pthread_mutex_t grid_mtx; // computational grid
pthread_mutex_t comp_flags_mtx;

// Threads
void *input_await(void *);
// define threads

void *event_handler(void *v);
bool send_to_pipe(message *msg);
/* Handler for SIGPIPE */
static void kill_from_pipe_error(int signum) {
  error("A pipe error occurred, quitting.");
  kill_program();
  return;
}

bool argparse(int argc, char *argv[]) {
  int args_left = 1;
  double c_re;
  double c_im;
  while (args_left < argc) {
    if (args_left + 1 < argc) {
      if (!strcmp(argv[args_left], "-h") ||
          !strcmp(argv[args_left], "--help")) {
        return false;
      }
      if (!strcmp(argv[args_left], "--in")) {
        global_data.pipe_in_fn = argv[++args_left];
      } else if (!strcmp(argv[args_left], "--out")) {
        global_data.pipe_out_fn = argv[++args_left];
      } else if (!strcmp(argv[args_left], "-n")) {
        if (!set_n(atoi(argv[++args_left]))) {
          return false;
        }
      } else if (!strcmp(argv[args_left], "-c")) {
        if (args_left + 2 < argc) {
          errno = 0;
          c_re = strtof(argv[args_left + 1], NULL);
          if (errno) {
            return false;
          }
          c_im = strtof(argv[args_left + 2], NULL);
          if (errno) {
            return false;
          }
          if (!set_c(c_re, c_im)) {
            return false;
          }
          args_left += 2;
        } else { // we need 2 numbers for the -c parameter
          return false;
        }
      }

      else {
        return false;
      }

    } else { // Not a pair argument
      return false;
    }
    args_left++;
  } // end while
  if (!(global_data.pipe_in_fn)) {
    global_data.pipe_in_fn = "/tmp/computational_module.out";
  }
  if (!(global_data.pipe_out_fn)) {
    global_data.pipe_out_fn = "/tmp/computational_module.in";
  }
  return true;
}

int main(int argc, char *argv[]) {

  if (!argparse(argc, argv)) {
    printf("%s", INFO_STRING);
    return 102;
  }

  info("Main thread started.");

  debug("Initializing signals");
  signal(SIGPIPE, kill_from_pipe_error);

  debug("Initializing mutexes");
  pthread_mutex_init(&quit_flag_mtx, NULL);
  pthread_mutex_init(&framebuffer_mtx, NULL);
  pthread_mutex_init(&pipe_in_mtx, NULL);
  pthread_mutex_init(&pipe_out_mtx, NULL);
  pthread_mutex_init(&grid_mtx, NULL);
  pthread_mutex_init(&comp_flags_mtx, NULL);

  debug("Starting threads.");
  pthread_create(&thread_pool[0], NULL, keyboard_thread, NULL);
  pthread_create(&thread_pool[1], NULL, input_await, NULL);
  pthread_create(&thread_pool[2], NULL, render_thread, NULL);
  pthread_create(&thread_pool[3], NULL, pipe_thread, NULL);

  debug("Initialising computation scheme");

  fprintf(stderr, "\rPress h for help.\n");

  event_handler(0);
  debug("Main is closing the threads.");

  // Announce our death
  error("Quitting...");

  for (int i = 0; i < THR_NUM; i++) {
    pthread_join(thread_pool[i], NULL);
  }

  debug("Closing computation scheme");

  info("Main thread killed.");
  return 0;
}

// Handles processing of client events
void *event_handler(void *v) {
  bool local_quit = should_quit();
  while (local_quit != true) {
    local_quit = should_quit();
    event processed_event = queue_pop();
    // debug("Processing an event with src %d type %d",
    // processed_event.source,
    //       processed_event.type);
    switch (processed_event.source) {
    case EV_KEYBOARD:
      switch (processed_event.type) {
      case EV_TYPE_NUM:
      case EV_THREAD_EXIT:
        break;
      case EV_ABORT:
        debug("Computation aborted. (user)");
        processed_event.data.msg = generate_message(MSG_ABORT);
        send_to_pipe(processed_event.data.msg);
        processed_event.data.msg = NULL;
        break;
      case EV_QUIT:
        debug("Received a kill signal!");
        local_quit = kill_program();
        break;
      case EV_GET_VERSION:
        processed_event.data.msg = generate_message(MSG_GET_VERSION);
        send_to_pipe(processed_event.data.msg);
        break;
      case EV_REFRESH:
        trash_framebuffer();
        break;
      case EV_COMPUTE:
        if (processed_event.data.msg == NULL) {
          processed_event.data.msg = generate_message(MSG_COMPUTE);
        }
        set_done(false);
        compute(processed_event.data.msg);
        send_to_pipe(processed_event.data.msg);
        break;
      case EV_SET_COMPUTE:
        if (processed_event.data.msg == NULL) {
          processed_event.data.msg = generate_message(MSG_SET_COMPUTE);
        }
        set_compute(processed_event.data.msg);
        send_to_pipe(processed_event.data.msg);
        break;
      default:
        error("Unknown event received of type %d.", processed_event.type);
      }
      break;        // end keyboard event processing
    case EV_NUCLEO: // All events from external sources
      switch (processed_event.type) {
      case EV_ABORT:
        warning("Computation aborted. (module)");
        abort_comp();
        break;
      case EV_QUIT:
        debug("Received a kill signal! from remote");
        local_quit = kill_program();
        break;
      case EV_PIPE_MSG_IN: // A common event for incoming remote messages
        switch (processed_event.data.msg->type) {
        case MSG_ABORT:
          debug("Computation aborted. (module)");
          abort_comp();

          break;
        case MSG_OK:
          info("OK");
          break;
        case MSG_ERROR:
          error("Compute module threw an error!");
          break;
        case MSG_VERSION:
          info("v%d.%d p%d", processed_event.data.msg->data.version.major,
               processed_event.data.msg->data.version.minor,
               processed_event.data.msg->data.version.patch);
          break;
        case MSG_COMPUTE_DATA:
          if (!is_computing()) { // In case we get raced
            break;
          } else {
            update_data(&(processed_event.data.msg->data.compute_data));
          }
          break;
        case MSG_STARTUP:
          info("%s", processed_event.data.msg->data.startup.message);
          break;
        case MSG_DONE:
          // A chunk is complete, do another one, if reasonable
          debug("chunk done!");
          if (is_computing()) { // Unless we are aborted / done, make a new
                                // job.
            processed_event.type = EV_COMPUTE;
            processed_event.source = EV_KEYBOARD;
            // EV_COMPUTE doesn't need a message, we need to free it here.
            free(processed_event.data.msg);
            processed_event.data.msg = NULL;
            queue_push(processed_event);
          }
          break;
        default:
          error("Unknown remote message of type %d.",
                processed_event.data.msg->type);
          break;
        }
        break; // end pipe_in message handling
      default:
        error("Unknown remote event received of type %d.",
              processed_event.type);
        break;
      }
      // we had to receive a message, let's free it now.
      if (processed_event.data.msg) {
        free(processed_event.data.msg);
      }
      break;
    default:
      break; // end remote event processing
    }        // end event processing (all)
  }

  queue_cleanup();
  info("Event handler died");
  return 0;
}

// Renders the symbol indicating process survival
void *input_await(void *v) {
  // As a pure aesthetics function I am allowing myself some magic!
  bool local_quit = false;
  int counter = 0;
  char states[5] = {'-', '/', '|', '\\', ' '};
  do {
    local_quit = should_quit();

    if (is_computing()) {
      fprintf(stderr, "\r%c-", states[counter = (counter + 1) % 4]);
      usleep(global_data.tick * 200);
    } else {
      fprintf(stderr, "\r%c>",
              states[(counter = ((counter + 1) % 4)) % 2 ? 0 : 4]);
      usleep(global_data.tick * 3500);
    }
  } while (local_quit != true);
  // we need to clear the buffer for whatever message we end up printing next
  fprintf(stderr, "\r         \r");
  return 0;
}
