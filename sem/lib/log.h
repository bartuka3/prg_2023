// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#ifndef PRG_LOG_H
#define PRG_LOG_H

#define NDEBUG

#define VERIFY(X) test(X, __func__, __LINE__, __FILE__) // Assert using test()

void print_msg(const char *format, const char *text, va_list args);

void error(const char *text, ...);
void warning(const char *text, ...);
void info(const char *text, ...);
void debug(const char *text, ...);

bool test(bool expression, const char *fcname, int line, const char *fname);
#endif // PRG_LOG_H
