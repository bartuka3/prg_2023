// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

/* The following files were used as a base:
 * Filename: prg-lec09-main.c
 * Date:     2019/12/25 21:38
 * Author:   Jan Faigl
 *
 * Thanks!
 */
#include "keyboard_utils.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

#include "event_queue.h"
#include "globals.h"
#include "log.h"
#include "prg_io_nonblock.h"
#include "render.h"

extern const char *HELP_STRING;

extern global_data_t global_data;
extern pthread_mutex_t quit_flag_mtx;
extern pthread_cond_t quit_signal;

void evaluate_command(char command) {
  event tmp_event = {}; // We have to always only use allocated memory here
  tmp_event.source = EV_KEYBOARD;
  tmp_event.type = EV_TYPE_NUM;
  switch (command) {
  case 'a':
    tmp_event.type = EV_ABORT;
    break;
  case 'q':
    debug("Telling the program to die.");
    tmp_event.type = EV_QUIT;
    break;
  case 'h':
    fprintf(stderr, "\r%s", HELP_STRING);
    break;
  case 'e':
    tmp_event.type = EV_REFRESH;
    break;
  case 'S':
    tmp_event.type = EV_SET_COMPUTE;
    break;
  case 'C':
    tmp_event.type = EV_COMPUTE;
    break;
  case 'v':
    tmp_event.type = EV_GET_VERSION;
    break;
  case '+':
    increase_scale(tmp_event);
    tmp_event.type = EV_TYPE_NUM; // we already sent an event
    break;
  case '-':
    decrease_scale(tmp_event);
    tmp_event.type = EV_TYPE_NUM; // we already sent an event
    break;
  default:
    warning("Unknown command: %c (Press 'h' for help)", command);
  }
  if (tmp_event.type != EV_TYPE_NUM && tmp_event.source) {
    queue_push(tmp_event);
  }
}

/* The keyboard thread. It handles the entirety of terminal input. */
void *keyboard_thread(void *v) {
  info("Keyboard thread started.");
  call_termios(0);
  // Set the default value for our quitting condition
  bool local_quit = should_quit();
  char read_char = '\0'; // we need to define c here beforehand
  while ((read_char != EOF) && !local_quit) {

    local_quit = should_quit();
    if (local_quit) { // in case we get raced here
      break;
    }
    if (io_getc_timeout(0, global_data.tick, (unsigned char *)&read_char)) {
      evaluate_command(read_char);
    }
  }

  call_termios(1);
  info("Keyboard thread stopped.");
  return 0;
}

/* Sets raw / normal terminal mode. */
void call_termios(int reset) {
  debug("Forced terminal into %s mode", reset ? "normal" : "raw");
  static struct termios tio, tioOld;
  tcgetattr(STDIN_FILENO, &tio);
  if (reset) {
    tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
  } else {
    tioOld = tio; // backup
    cfmakeraw(&tio);
    tio.c_oflag |= OPOST;
    tcsetattr(STDIN_FILENO, TCSANOW, &tio);
  }
}
