// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023
// This file was heavily inspired by the b3b36prg prgsem tutorials, thanks!

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "event_queue.h"
#include "globals.h"
#include "log.h"

#ifndef QUEUE_CAPACITY
#define QUEUE_CAPACITY 16
#endif

#ifndef NDEBUG_QUEUE
#define NDEBUG_QUEUE
#endif

typedef struct {
  event queue[QUEUE_CAPACITY];
  int head;
  int tail;

  pthread_mutex_t queue_mutex;
  pthread_cond_t queue_signal;
  bool quit_flag;
} queue;

static queue global_queue = {.head = 0, .tail = 0};

void queue_init(void) {
  pthread_mutex_init(&(global_queue.queue_mutex), NULL);
  pthread_cond_init(&(global_queue.queue_signal), NULL);
}
void queue_cleanup(void) {
  while (global_queue.head != global_queue.tail) {
    event local_event = queue_pop();
    if (local_event.data.msg != NULL) {
      // We need to remove the msg
      free(local_event.data.msg);
    }
  }
}

event queue_pop(void) {
  event return_event = {.type = EV_TYPE_NUM};
  pthread_mutex_lock(&(global_queue.queue_mutex));
  while (!global_queue.quit_flag && global_queue.head == global_queue.tail) {
    // This frees the lock and waits for push, we are empty
    pthread_cond_wait(&(global_queue.queue_signal),
                      &(global_queue.queue_mutex));
  }
  // We need to check if we aren't just quitting
  if (global_queue.head != global_queue.tail) {
    return_event = global_queue.queue[global_queue.tail];
    global_queue.tail = (global_queue.tail + 1) % QUEUE_CAPACITY;
    pthread_cond_broadcast(&(global_queue.queue_signal));
  }

  pthread_mutex_unlock(&(global_queue.queue_mutex));
#ifndef NDEBUG_QUEUE
  debug("Removing an event with src %d type %d", return_event.source,
        return_event.type);
#endif
  return return_event;
}

void queue_push(event ev) {
#ifndef NDEBUG_QUEUE
  debug("Adding an event with src %d type %d", ev.source, ev.type);
#endif
  if (should_quit() && ev.data.msg) {
    free(ev.data.msg);
    return;
  }
  pthread_mutex_lock(&(global_queue.queue_mutex));
  while ((global_queue.head + 1) % QUEUE_CAPACITY == global_queue.tail) {
    // This frees the lock and waits for pop, we are full
    pthread_cond_wait(&(global_queue.queue_signal),
                      &(global_queue.queue_mutex));
  }
  global_queue.queue[global_queue.head] = ev;
  global_queue.head = (global_queue.head + 1) % QUEUE_CAPACITY;

  pthread_cond_broadcast(&(global_queue.queue_signal));
  pthread_mutex_unlock(&(global_queue.queue_mutex));
}

bool get_quit_state() {
  pthread_mutex_lock(&(global_queue.queue_mutex));
  bool quit_flag = global_queue.quit_flag;
  pthread_mutex_unlock(&(global_queue.queue_mutex));
  return quit_flag;
}
void set_queue_quit_state(bool state) {
  debug("setting queue kill state");
  pthread_mutex_lock(&(global_queue.queue_mutex));
  global_queue.quit_flag = state;
  pthread_mutex_unlock(&(global_queue.queue_mutex));
  pthread_cond_broadcast(&(global_queue.queue_signal));
  debug("successfully set queue kill state");
}

event generate_simple_event(int source, int type) {
  // Generates an empty event with only the source and type filled out
  event tmp_event = {};
  /*if (tmp_event == NULL) {
    error("MEMORY ALLOCATION ERROR");
    return NULL;
  }*/
  tmp_event.source = source;
  tmp_event.type = type;
  return tmp_event;
}