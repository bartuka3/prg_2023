// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include "render.h"

#include "compute_handler.h"
#include "event_queue.h"
#include "globals.h"
#include "log.h"
#include "xwin_sdl.h"

#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

#include <SDL.h>
#include <unistd.h>

#include <SDL_image.h>

// Non-magical CONSTANTS
const int DEFAULT_WINDOW_WIDTH = 160;
const int DEFAULT_WINDOW_HEIGHT = 120;
SDL_DisplayMode DisplayMode; // To determine the screen size

// External structs
extern global_data_t global_data;
extern framebuffer_t GLOBAL_FRAMEBUFFER;

// External mutexes
extern pthread_mutex_t framebuffer_mtx;
extern pthread_mutex_t quit_flag_mtx;

framebuffer_t GLOBAL_FRAMEBUFFER = {
    .invalid = false, // Marks that we want to reload the window
    .width = DEFAULT_WINDOW_WIDTH,
    .height = DEFAULT_WINDOW_HEIGHT,
    .scale = 1 // Integer scaling
};

bool new_window_could_fit(int scale) {
  SDL_GetCurrentDisplayMode(0, &DisplayMode);
  int w = DEFAULT_WINDOW_WIDTH * scale;
  int h = DEFAULT_WINDOW_HEIGHT * scale;
  debug("scr size %dx%d wanna move to %dx%d", DisplayMode.h, DisplayMode.w, h,
        w);
  debug("%b  %b  %b  %b  %b ", scale > 2, w > DEFAULT_WINDOW_WIDTH,
        w<DisplayMode.w, h> DEFAULT_WINDOW_HEIGHT, h < DisplayMode.h);
  return scale > 1 && w > DEFAULT_WINDOW_WIDTH && w < DisplayMode.w &&
         h > DEFAULT_WINDOW_HEIGHT && h < DisplayMode.h;
}

void increase_scale(event tmp_event) {
  if (new_window_could_fit(GLOBAL_FRAMEBUFFER.scale + 1)) {

    pthread_mutex_lock(&framebuffer_mtx);
    GLOBAL_FRAMEBUFFER.invalid = true; // restarts the render thread
    GLOBAL_FRAMEBUFFER.scale++;
    GLOBAL_FRAMEBUFFER.width = DEFAULT_WINDOW_WIDTH * GLOBAL_FRAMEBUFFER.scale;
    GLOBAL_FRAMEBUFFER.height =
        DEFAULT_WINDOW_HEIGHT * GLOBAL_FRAMEBUFFER.scale;
    pthread_mutex_unlock(&framebuffer_mtx);

    // Any and all concurrent computations are now invalid
    tmp_event = generate_simple_event(EV_KEYBOARD, EV_ABORT);
    queue_push(tmp_event);
  }
  return;
}

void decrease_scale(event tmp_event) {
  if (new_window_could_fit(GLOBAL_FRAMEBUFFER.scale - 1)) {
    pthread_mutex_lock(&framebuffer_mtx);
    GLOBAL_FRAMEBUFFER.invalid = true; // restarts the render thread
    GLOBAL_FRAMEBUFFER.scale--;
    // to prevent unwanted downsizes
    GLOBAL_FRAMEBUFFER.width = DEFAULT_WINDOW_WIDTH * GLOBAL_FRAMEBUFFER.scale;
    GLOBAL_FRAMEBUFFER.height =
        DEFAULT_WINDOW_HEIGHT * GLOBAL_FRAMEBUFFER.scale;

    pthread_mutex_unlock(&framebuffer_mtx);

    // Any and all concurrent computations are now invalid
    tmp_event = generate_simple_event(EV_KEYBOARD, EV_ABORT);
    queue_push(tmp_event);
  }
}

void process_SDL_event(SDL_Event caught_event) {
  event tmp_event;
  switch (caught_event.type) {
  case SDL_QUIT:
    debug("Window said it's time to die");
    tmp_event = generate_simple_event(EV_KEYBOARD, EV_QUIT);
    queue_push(tmp_event);
    break;
  case SDL_KEYDOWN:
    switch (caught_event.key.keysym.sym) {

    case SDLK_a:
      tmp_event = generate_simple_event(EV_KEYBOARD, EV_ABORT);
      queue_push(tmp_event);
      break;
    case SDLK_c:
      tmp_event = generate_simple_event(EV_KEYBOARD, EV_COMPUTE);
      queue_push(tmp_event);
      break;
    case SDLK_e:
      tmp_event = generate_simple_event(EV_KEYBOARD, EV_REFRESH);
      queue_push(tmp_event);
      break;
    case SDLK_q:
      debug("User told the window it's time to die");
      tmp_event = generate_simple_event(EV_KEYBOARD, EV_QUIT);
      queue_push(tmp_event);
      break;
    case SDLK_s:
      tmp_event = generate_simple_event(EV_KEYBOARD, EV_SET_COMPUTE);
      queue_push(tmp_event);
      break;
    case SDLK_v:
      tmp_event = generate_simple_event(EV_KEYBOARD, EV_GET_VERSION);
      queue_push(tmp_event);
      break;

    case SDLK_KP_PLUS:
      increase_scale(tmp_event);
      break;
    case SDLK_KP_MINUS:
      decrease_scale(tmp_event);
      break;
    }
  default:
    break;
  }
}

void poll_SDL_event() {
  SDL_Event polled_event;
  if (SDL_PollEvent(&polled_event)) {
    process_SDL_event(polled_event);
  }
}

// Creates a new blank uint8_t *img
unsigned char *create_window() {
  VERIFY(GLOBAL_FRAMEBUFFER.width && GLOBAL_FRAMEBUFFER.height);
  unsigned char *img =
      calloc(GLOBAL_FRAMEBUFFER.width * GLOBAL_FRAMEBUFFER.height * 3,
             sizeof(unsigned char));
  if (img == NULL) {
    error("MEMORY ALLOCATION ERROR");
  }
  debug("Allocated the window.");
  return img;
}

// Removes the current fb & comp. grid and replaces it with a blank one
void trash_framebuffer() {
  computation_cleanup();
  pthread_mutex_lock(&framebuffer_mtx);
  free(GLOBAL_FRAMEBUFFER.img);
  GLOBAL_FRAMEBUFFER.img = create_window();
  pthread_mutex_unlock(&framebuffer_mtx);
  abort_comp();
  computation_init();
}

// replaces the current fb with the argument
void replace_buffer(int w, int h, uint8_t *img) {
  pthread_mutex_lock(&framebuffer_mtx);
  VERIFY(w == GLOBAL_FRAMEBUFFER.width && h == GLOBAL_FRAMEBUFFER.height);
  VERIFY(GLOBAL_FRAMEBUFFER.img != NULL);
  free(GLOBAL_FRAMEBUFFER.img);
  GLOBAL_FRAMEBUFFER.img = img;
  pthread_mutex_unlock(&framebuffer_mtx);
}

void *render_thread(void *v) {
  // Init window
render_start:
  computation_init();
  pthread_mutex_lock(&framebuffer_mtx);
  info("Creating a window.");
  if (xwin_init(GLOBAL_FRAMEBUFFER.width, GLOBAL_FRAMEBUFFER.height) < 0) {
    error("ERROR INITIALISING");
  }
  GLOBAL_FRAMEBUFFER.img = create_window();
  // TODO replace this test with actual initialization
  // debug("Writing into the window %p", (void *)GLOBAL_FRAMEBUFFER.img);
  /*for (int x = 0; x < GLOBAL_FRAMEBUFFER.width; x++) {
    for (int y = 0; y < GLOBAL_FRAMEBUFFER.height; y++) {
      GLOBAL_FRAMEBUFFER.img[(y * GLOBAL_FRAMEBUFFER.width + x) * 3] =
          (x) % 256;
      GLOBAL_FRAMEBUFFER.img[(y * GLOBAL_FRAMEBUFFER.width + x) * 3 + 1] =
          (y) % 256;
    }
  }*/
  pthread_mutex_unlock(&framebuffer_mtx);
  debug("Written into the img, blitting.");
  bool local_quit = should_quit();

  GLOBAL_FRAMEBUFFER.invalid = false;

  int counter = 0;
  while (!local_quit && !GLOBAL_FRAMEBUFFER.invalid) {
    // TODO actual rendering code
    counter++;
    /*pthread_mutex_lock(&framebuffer_mtx);
    counter %= (GLOBAL_FRAMEBUFFER.width * GLOBAL_FRAMEBUFFER.height * 3);
    pthread_mutex_unlock(&framebuffer_mtx);

    */
    local_quit = should_quit(); // See if we need to restart
    update_framebuffer();
    pthread_mutex_lock(&framebuffer_mtx);
    xwin_redraw(GLOBAL_FRAMEBUFFER.width, GLOBAL_FRAMEBUFFER.height,
                GLOBAL_FRAMEBUFFER.img);

    // GLOBAL_FRAMEBUFFER.img[counter] = counter % 250;
    pthread_mutex_unlock(&framebuffer_mtx);

    // Check events
    poll_SDL_event();
    if (GLOBAL_FRAMEBUFFER.invalid) {
      info("Reloading!");
      local_quit = true;
      break;
    }

    usleep(200);
  }
  xwin_close();
  // free our framebuffer
  pthread_mutex_lock(&framebuffer_mtx);
  free(GLOBAL_FRAMEBUFFER.img);
  GLOBAL_FRAMEBUFFER.img = NULL;
  pthread_mutex_unlock(&framebuffer_mtx);
  computation_cleanup();
  // Check if we wanted to actually just reload the window.
  if (GLOBAL_FRAMEBUFFER.invalid) {
    goto render_start; // I know this is lazy. But it works!
  }
  info("render thread died");

  return 0;
}
