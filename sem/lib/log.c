// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include "log.h"
#include "keyboard_utils.h"
#include <stdio.h>
#include <stdlib.h>
#define OUTPUT_BUFFER stderr
// We hardcode the print limit to avoid dynamic allocation and text spam
// Admittedly, this constant is rather arbitrary
#define ERROR_LENGTH 300

void print_msg(const char *format, const char *text, va_list args) {
  char error_msg[ERROR_LENGTH];
  vsnprintf(error_msg, ERROR_LENGTH, text, args);
  fprintf(OUTPUT_BUFFER, format, error_msg);
}

void error(const char *text, ...) {
  va_list ap;
  va_start(ap, text);
  print_msg("\r\033[41mERROR: %s\033[49m\n", text, ap);
  va_end(ap);
}
void warning(const char *text, ...) {
  va_list ap;
  va_start(ap, text);
  print_msg("\r\033[103mWARN:\033[49m %s\n", text, ap);
  va_end(ap);
}
void info(const char *text, ...) {
  va_list ap;
  va_start(ap, text);
  print_msg("\r\033[44mINFO:\033[49m %s\n", text, ap);
  va_end(ap);
}
void debug(const char *text, ...) {
#ifdef NDEBUG
  return;
#endif
  va_list ap;
  va_start(ap, text);
  print_msg("\r\033[32mDEBUG: %s\033[39m\n", text, ap);
  va_end(ap);
}

bool test(bool expression, const char *fcname, int line, const char *fname) {
  if (!expression) {
    error("Assert failed in %s on line %d in file %s.", fcname, line, fname);
    call_termios(1); // reset keyboard state
    exit(105);
  }
  return expression;
}