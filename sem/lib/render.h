// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

//
// Created by krab on 3.5.23.
//

#ifndef PRG_RENDER_H
#define PRG_RENDER_H
#include "event_queue.h"
#include <SDL.h>
void *render_thread(void *v);

// Processes a polled SDL event
void process_SDL_event(SDL_Event polled_event);

// Polls an SDL event, if it manages to catch one
void poll_SDL_event();

// Creates an empty framebuffer
unsigned char *create_window();

void trash_framebuffer();
void replace_buffer(int w, int h, uint8_t *img);

// For managing the window size
void increase_scale(event tmp_event);
void decrease_scale(event tmp_event);
#endif // PRG_RENDER_H
