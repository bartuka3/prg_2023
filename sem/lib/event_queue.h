// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023
// This file was heavily inspired by the b3b36prg prgsem tutorials, thanks!

#ifndef __EVENT_QUEUE_H__
#define __EVENT_QUEUE_H__

#include "messages.h"

#define EVENT_QUEUE_SIZE 32

typedef enum { EV_NUCLEO, EV_KEYBOARD, EV_NUM } event_source;

typedef enum {
  EV_COMPUTE,     // request compute on nucleo with particular
  EV_RESET_CHUNK, // reset the chunk id
  EV_ABORT,
  EV_GET_VERSION,
  EV_THREAD_EXIT,
  EV_QUIT,
  EV_SERIAL,
  EV_SET_COMPUTE,
  EV_COMPUTE_CPU,
  EV_CLEAR_BUFFER,
  EV_REFRESH,
  EV_TYPE_NUM,
  EV_PIPE_MSG_IN
} event_type;

typedef struct {
  int param;
} event_keyboard;

typedef struct {
  message *msg;
} event_serial;

typedef struct {
  event_source source;
  event_type type;
  union {
    int param;
    message *msg;
  } data;
} event;

void queue_init(void);
void queue_cleanup(void);

event queue_pop(void);

void queue_push(event ev);

bool get_quit_state();
void set_queue_quit_state(bool state);
void set_destroy_state(bool quit);
event generate_simple_event(int source, int type);
#endif

/* end of event_queue.h */
