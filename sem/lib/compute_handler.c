// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include "compute_handler.h"
#include "../lib/event_queue.h"
#include "../lib/globals.h"
#include "../lib/log.h"
#include "../lib/messages.h"

#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_ERRORS                                                             \
  5 // this is magical and only here for the out-of-order errors, to prevent
    // errors from spamming the user in case we call compute multiple times

// Most of this code is stolen from the tutorial

// All of these functions can be ever called from the event_handler so we don't
// need to MT-protect them most of the time.

extern framebuffer_t GLOBAL_FRAMEBUFFER; // for dynamic size, we don't actually
                                         // write into the video memory in here.
extern pthread_mutex_t framebuffer_mtx;
extern pthread_mutex_t grid_mtx;
extern pthread_mutex_t comp_flags_mtx;

const int default_chunk_grid_size =
    10; // How many chunks there will be on one axis, the higher the number the
        // more chunks will be generated

static struct {
  double c_re;
  double c_im;
  int n;

  double range_re_min;
  double range_re_max;
  double range_im_min;
  double range_im_max;

  int grid_w;
  int grid_h;

  int cur_x;
  int cur_y;

  double d_re;
  double d_im;

  int nbr_chunks;
  int cid;
  double chunk_re;
  double chunk_im;

  uint8_t chunk_grid_size;
  uint8_t chunk_n_re;
  uint8_t chunk_n_im;

  uint8_t *grid;
  bool computing;
  bool done;

  int error_raised;

} comp = {.c_re = -0.4,
          .c_im = 0.6,
          .n = 80,
          .range_re_min = -1.6,
          .range_re_max = 1.6,
          .range_im_min = -1.1,
          .range_im_max = 1.1,

          .grid = NULL,  // overwritten
          .grid_w = 640, // overwritten
          .grid_h = 480, // overwritten

          .chunk_n_re = 64, // overwritten
          .chunk_n_im = 48, // overwritten

          .computing = false,
          .done = false,

          .error_raised = 0};

void computation_init(void) {

  comp.cid = 0;
  comp.done = false;
  comp.computing = false;

  pthread_mutex_lock(&framebuffer_mtx);
  comp.chunk_grid_size = 10;
  comp.grid_w = GLOBAL_FRAMEBUFFER.width;
  comp.grid_h = GLOBAL_FRAMEBUFFER.height;
  comp.chunk_n_re = comp.grid_w / (comp.chunk_grid_size);
  comp.chunk_n_im = comp.grid_h / (comp.chunk_grid_size);
  pthread_mutex_unlock(&framebuffer_mtx);
  VERIFY(!(comp.grid_w % comp.chunk_grid_size));
  VERIFY(!(comp.grid_h % comp.chunk_grid_size));
  pthread_mutex_lock(&grid_mtx);
  if (comp.grid != NULL) {
    free(comp.grid);
  }
  comp.grid = calloc(sizeof(uint8_t), comp.grid_w * comp.grid_h);
  pthread_mutex_unlock(&grid_mtx);

  comp.d_re = (comp.range_re_max - comp.range_re_min) / (1. * comp.grid_w);
  comp.d_im = (comp.range_im_max - comp.range_im_min) / (1. * comp.grid_h);

  comp.nbr_chunks =
      (comp.grid_w * comp.grid_h) / (comp.chunk_n_re * comp.chunk_n_im);

  debug("Comp module: %d %d : dx=%f dy=%f : %d chunks", comp.chunk_n_re,
        comp.chunk_n_im, comp.d_re, comp.d_im, comp.nbr_chunks);
  debug("comp initialized");
}
void computation_cleanup(void) {
  if (comp.grid) {
    free(comp.grid);
  }
  comp.grid = NULL;
}

bool is_computing(void) {
  pthread_mutex_lock(&comp_flags_mtx);
  bool c = comp.computing;
  pthread_mutex_unlock(&comp_flags_mtx);
  return c;
}
bool is_done(void) {
  pthread_mutex_lock(&comp_flags_mtx);
  bool d = comp.done;
  pthread_mutex_unlock(&comp_flags_mtx);
  return d;
}
void set_computing(bool value) {
  pthread_mutex_lock(&comp_flags_mtx);
  comp.computing = value;
  pthread_mutex_unlock(&comp_flags_mtx);
}
void set_done(bool value) {
  pthread_mutex_lock(&comp_flags_mtx);
  comp.done = value;
  pthread_mutex_unlock(&comp_flags_mtx);
}

void abort_comp(void) { set_computing(false); }

bool set_compute(message *msg) {
  bool ret = !is_computing();
  VERIFY(msg != NULL);
  if (ret) {
    msg->type = MSG_SET_COMPUTE;
    msg->data.set_compute.c_re = comp.c_re;
    msg->data.set_compute.c_im = comp.c_im;
    msg->data.set_compute.d_re = comp.d_re;
    msg->data.set_compute.d_im = comp.d_im;
    msg->data.set_compute.n = comp.n;
    comp.done = false;
  }
  return ret;
}

bool compute(message *msg) {
  VERIFY(msg != NULL);
  VERIFY(comp.d_re && comp.d_im);

  if (!is_computing()) {   // first chunk
    comp.error_raised = 0; // reset the error count
    comp.cid = 0;
    comp.computing = true;
    comp.cur_x = comp.cur_y = 0;       // start with the first rect
    comp.chunk_re = comp.range_re_min; // left side
    comp.chunk_im = comp.range_im_min; // upper side
    msg->type = MSG_COMPUTE;
  } else { // next chunk
    comp.cid += 1;
    info("chunk %d / %d", comp.cid + 1, comp.nbr_chunks);
    if (comp.cid < comp.nbr_chunks) {
      comp.cur_x += comp.chunk_n_re;
      comp.chunk_re += comp.chunk_n_re * comp.d_re;
      if (comp.cur_x >= comp.grid_w) {
        comp.chunk_re = comp.range_re_min;
        comp.chunk_im += comp.chunk_n_im * comp.d_im;
        comp.cur_x = 0;
        comp.cur_y += comp.chunk_n_im;
      }
      msg->type = MSG_COMPUTE;
    } else { // finished
      msg->type = MSG_NBR;
      abort_comp();
      return is_computing();
    }
  }
  if (is_computing() && msg->type == MSG_COMPUTE) {
    debug("sending message %f %f :> %d %d", comp.chunk_re, comp.chunk_im,
          comp.chunk_n_re, comp.chunk_n_im);
    msg->data.compute.cid = comp.cid;
    msg->data.compute.re = comp.chunk_re;
    msg->data.compute.im = comp.chunk_im;
    msg->data.compute.n_re = comp.chunk_n_re;
    msg->data.compute.n_im = comp.chunk_n_im;
  }
  return is_computing();
}

void update_data(const msg_compute_data *compute_data) {
  VERIFY(compute_data != NULL);
  if (compute_data->cid != comp.cid && comp.error_raised < MAX_ERRORS) {
    warning("Received chunk out of order with id %d.", compute_data->cid);
    comp.error_raised++; // increase the error count
  }
  const int idx = comp.cur_x + compute_data->i_re +
                  (comp.cur_y + compute_data->i_im) * comp.grid_w;
  if (idx >= 0 && idx < (comp.grid_w * comp.grid_h)) {
    pthread_mutex_lock(&grid_mtx);
    comp.grid[idx] = compute_data->iter;
    pthread_mutex_unlock(&grid_mtx);
  } else {
    warning("Attempted to render outside of the buffer");
    set_done(true);
  }
  if ((comp.cid + 1) >= comp.nbr_chunks &&
      (compute_data->i_re + 1) == comp.chunk_n_re &&
      (compute_data->i_im + 1) == comp.chunk_n_im) {
    comp.done = true;
    comp.computing = false;
  }
}

void update_image(int w, int h, unsigned char *img) {
  for (int i = 0; i < w * h; i++) {
    pthread_mutex_lock(&grid_mtx);
    const double t = (1. * comp.grid[i]) / (comp.n + 1.0);
    pthread_mutex_unlock(&grid_mtx);
    *(img++) = 9 * (1 - t) * t * t * t * 255;
    *(img++) = 15 * (1 - t) * (1 - t) * t * t * 255;
    *(img++) = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
  }
}

void update_framebuffer() {
  pthread_mutex_lock(&framebuffer_mtx);
  update_image(GLOBAL_FRAMEBUFFER.width, GLOBAL_FRAMEBUFFER.height,
               GLOBAL_FRAMEBUFFER.img);
  pthread_mutex_unlock(&framebuffer_mtx);
}

// Parameter setting functions
bool set_n(int n) {
  if (n) {
    comp.n = n;
    return true;
  } else {
    return false;
  }
}

bool set_c(double c_re, double c_im) {
  if (errno) {
    return false;
  }
  comp.c_re = c_re;
  comp.c_im = c_im;
  debug("set c to %f +%fi.", c_re, c_im);
  return true;
}