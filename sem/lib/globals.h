// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

//
// Created by krab on 29.4.23.
//

#ifndef PRG_GLOBALS_H
#define PRG_GLOBALS_H
#include "messages.h"
#include <stdbool.h>

typedef struct {
  bool quit_flag;
  bool framebuffer_invalid;
  const char *pipe_in_fn;
  const char *pipe_out_fn;
  int pipe_in_fd;
  int pipe_out_fd;
  int tick;
  int pipe_tick;
  bool do_work;
} global_data_t;

typedef struct {
  bool invalid;
  int width;
  int height;
  unsigned char *img;
  int scale;
} framebuffer_t;

bool should_quit(void);
bool kill_program(void);

#endif // PRG_GLOBALS_H
