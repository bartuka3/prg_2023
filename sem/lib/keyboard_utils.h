// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#ifndef PRG_KEYBOARD_UTILS_H
#define PRG_KEYBOARD_UTILS_H

void call_termios(int reset);
void *keyboard_thread(void *v);
#endif // PRG_KEYBOARD_UTILS_H
