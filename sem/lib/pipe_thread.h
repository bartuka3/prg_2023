// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#ifndef PRG_PIPE_THREAD_H
#define PRG_PIPE_THREAD_H
void act_upon_message(message *msg);
bool send_to_pipe(message *msg);

/* Handler for SIGPIPE */ /*
 static void kill_from_pipe_error(int signum);*/

void *pipe_thread(void *v);
#endif // PRG_PIPE_THREAD_H
