// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#ifndef PRG_COMPUTE_H
#define PRG_COMPUTE_H

#include "../lib/messages.h"
#include <stdbool.h>

void computation_init(void);
void computation_cleanup(void);

bool is_computing(void);
bool is_done(void);
void set_computing(bool value);
void set_done(bool value);

void abort_comp(void);

bool set_compute(message *msg);
bool compute(message *msg);

void update_data(const msg_compute_data *compute_data);

void update_image(int w, int h, unsigned char *img);
void update_framebuffer();

// parameter assignment
bool set_n(int n);
bool set_c(double c_re, double c_im);
#endif // PRG_COMPUTE_H
