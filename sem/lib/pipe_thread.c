// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include <pthread.h>
#include <stdlib.h>

#include "event_queue.h"
#include "globals.h"
#include "log.h"
#include "messages.h"
#include "pipe_thread.h"
#include "prg_io_nonblock.h"

extern pthread_mutex_t quit_flag_mtx;
extern global_data_t global_data;

#ifndef NDEBUG_PIPE
#define NDEBUG_PIPE
#endif

int msg_type_to_event_type(message_type msg_type) {
  switch (msg_type) {
  case MSG_ABORT:
    return EV_ABORT;
  case MSG_COMPUTE:
    return EV_COMPUTE;
  case MSG_SET_COMPUTE:
    return EV_SET_COMPUTE;
  case MSG_GET_VERSION:
    return EV_GET_VERSION;
  default:
    return EV_PIPE_MSG_IN;
  }
}

void push_event_from_msg(message *msg, event_source ev_source) {
  event tmp_event;
  tmp_event.source = ev_source;
  tmp_event.type = msg_type_to_event_type(msg->type);
  tmp_event.data.msg = msg;

  queue_push(tmp_event);
  return;
}
/*
 */
/* Handler for SIGPIPE */ /*
static void kill_from_pipe_error(int signum) {
 error("A pipe error occured, quitting.");
 pthread_mutex_lock(&quit_flag_mtx);
 global_data.quit_flag = true;
 pthread_mutex_unlock(&quit_flag_mtx);
 return;
}*/

void *pipe_thread(void *v) {
  info("Pipe read thread started");
  global_data.pipe_in_fd = io_open_read(global_data.pipe_in_fn);
  if (global_data.pipe_in_fd == -1) {
    warning("Cannot open '%s' for reading!", global_data.pipe_in_fn);
  }
  unsigned char buffer = '\0';
  uint8_t *message_buffer = NULL;
  while (!should_quit() && global_data.pipe_in_fd != -1) {
    // Waits for a new message
    if (io_getc_timeout(global_data.pipe_in_fd, global_data.pipe_tick,
                        &buffer)) {
#ifndef NDEBUG_PIPE
      debug("got new %u <", (unsigned int)buffer);
#endif
      // create msg
      int message_length = 1;
      get_message_size(buffer, &message_length);
      message_buffer =
          reallocarray(message_buffer, message_length, sizeof(uint8_t));
      if (message_buffer == NULL) {
        error("Memory allocation error");
        break;
      }
      message_buffer[0] = buffer;
#ifndef NDEBUG_PIPE
      debug("time to fill the buffer");
#endif
      // fill the rest
      int i = 1;
      for (; i < message_length; i++) {
        while (!should_quit() &&
               !io_getc_timeout(global_data.pipe_in_fd, global_data.pipe_tick,
                                &buffer)) {
        }
        // now we either quit or write the buffer
        message_buffer[i] = buffer;
        if (should_quit()) {
          break;
        }
      }

      message *tmp_msg = calloc(1, sizeof(message));
#ifndef NDEBUG_PIPE
      debug("Time to parse");
#endif
      // we need to check we actually loaded all the bytes.
      if (i == message_length &&
          parse_message_buf(message_buffer, message_length, tmp_msg)) {
        push_event_from_msg(tmp_msg, EV_NUCLEO);

#ifndef NDEBUG_PIPE
        debug("parsed message successfully");
#endif
      } else {
        error("Cannot parse message.");
        // We failed to send the message, so we get rid of whatever's left
        free(tmp_msg);
      }
      // we free the temporary uint8 message buffer but not the msg itself.
      free(message_buffer);
      message_buffer = NULL;
    } // END character receive

  } // END while not quitting / pipe closed
  io_close(global_data.pipe_in_fd);
  global_data.pipe_in_fd = -1;
  info("Pipe read thread stopped.");
  return 0;
}

/* Sends a message to the pipe, destroying it in the process */
bool send_to_pipe(message *msg) {
  if (global_data.pipe_out_fd == -1) {
    global_data.pipe_out_fd = io_open_write(global_data.pipe_out_fn);
  }
  if (global_data.pipe_out_fd == -1) { // pipe_out invalid
    warning("Cannot open '%s' for writing!", global_data.pipe_out_fn);
    free(msg);
    msg = NULL;
    return false;
  }
#ifndef NDEBUG_PIPE
  warning("sending message type %d", msg->type);
#endif
  int message_length = 0;
  if (get_message_size(msg->type, &message_length)) {
    uint8_t *buffer = calloc(message_length + 1, sizeof(uint8_t));
    int to_send = message_length;
    if (fill_message_buf(msg, buffer, sizeof(*msg), &to_send)) {

      // now send each of the bytes to the pipe
      for (int i = 0; i < to_send; i++) {
        // fprintf(stderr, " %d ", buffer[i]);
        if (!io_putc(global_data.pipe_out_fd, buffer[i])) {
          free(buffer);
          free(msg);
          msg = NULL;
          return false;
        }
      }

      free(buffer);
      free(msg);
      msg = NULL;
      return true;

    } else { // Error while filling
      error("Cannot fill the buffer");
      free(buffer);
      free(msg);
      msg = NULL;
      return false;
    }
  } else { // error while getting message length
    error("We cannot send an invalid message");
    free(msg);
    msg = NULL;
    return false;
  }
}
