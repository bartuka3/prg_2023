// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include "globals.h"
#include <pthread.h>
#include <stdio.h>

extern global_data_t global_data;
extern pthread_mutex_t quit_flag_mtx;

const char *HELP_STRING =
    ""
    "\033[45mHelp\033[49m\n"
    "Press any of the following keys:\n"
    "h - display this help\n"
    "q - quit this program\n"
    "v - print out the Compute Module version\n"
    "S - submit the current settings to the Compute Module\n"
    "e - erase the current calculated buffer\n"
    "C - start computation\n"
    "a - pause the calculation\n"
    "+ - increase the window size\n"
    "- - decrease the window size\n";

const char *INFO_STRING =
    "prgsem-control [--in filename] [--out filename] [-n N] [-c REAL IMAG]\n"
    "\n"
    "optional arguments:\n"
    "    -h, --help      display this text and exit\n"
    "    --in filename   set the input pipe path for the control application\n"
    "    --out filename  set the output pipe path for the control application\n"
    "    -n N            set the number of iterations used (also, depth)\n"
    "    -c REAL IMAG    set the 'c' constant, both numbers are required\n";

bool should_quit() {
  pthread_mutex_lock(&quit_flag_mtx);
  bool local_quit = global_data.quit_flag;
  pthread_mutex_unlock(&quit_flag_mtx);
  return local_quit;
}

bool kill_program() {
  pthread_mutex_lock(&quit_flag_mtx);
  global_data.quit_flag = true;
  pthread_mutex_unlock(&quit_flag_mtx);
  return true;
}
