# PRG-SEM

*bartuka3, (c) 2023*

# Control application

## Usage

```
prgsem-control [--in filename] [--out filename] [-n N] [-c REAL IMAG]

optional arguments:
    -h, --help      display this text and exit
    --in filename   set the input pipe path for the control application
    --out filename  set the output pipe path for the control application
    -n N            set the number of iterations used (also, depth)
    -c REAL IMAG    set the "c" constant, both numbers are required
```

## Compilation

```shell
cd control_gui
make
chmod +x ./prgsem-control
./prgsem-control
```

## Controls:

### 1. In terminal

Press any of the following keys (case **sensitive**):

- `a` - abort the calculation, if one is currently running
- `C` - start computation
- `e` - clean the screen
- `h` - display this help
- `q` - quit the program
- `s` - submit the current settings to the Compute Module
- `v` - print out the Compute Module version
- `+` - increase the window size
- `-` - decrease the window size

### 1. In window

Press any of the following keys (case **in**sensitive):

- `a` - abort the calculation, if one is currently running
- `c` - start computation
- `e` - clean the screen
- `q` - quit this program
- `s` - submit the current settings to the Compute Module
- `v` - print out the Compute Module version
- `+` - increase the window size
- `-` - decrease the window size

## Features

- (10p) Basic implementation according
  to [the specification.](https://cw.fel.cvut.cz/wiki/courses/b3b36prg/semestral-project/start#zakladni_implementace)
- (2p) Render Window gets refreshed and the image sustains being covered by another program
- (2p) The application reacts to commands on both the terminal and the window
- (2p) The window can be dynamically resized using a window shortcut (integer scaling, will prevent illegal window sizes
  for the current screen and updates the number of chunks to accommodate the new resolution accordingly)
- (1p) Program correctly handles pipe-related signals (such as SIGPIPE)

## Notes

- You might need to provide a compute module in order to use the control application
- The behaviour of starting the computation before executing the set compute command (`s`) is undefined.
- Redrawing and resuming computation after resizing the window **after** starting computation is undefined and may lead
  to unexpected outcomes, but may work, provided you send the Compute Module the updated information.
- You might have to press the abort command multiple times because of the design of the computational module
