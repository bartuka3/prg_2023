// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023
#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../lib/compute_handler.h"
#include "../lib/event_queue.h"
#include "../lib/globals.h"
#include "../lib/keyboard_utils.h"
#include "../lib/log.h"
#include "../lib/pipe_thread.h"

// Mutexes
pthread_mutex_t quit_flag_mtx;
pthread_mutex_t event_queue_mtx;
pthread_cond_t exit_signal;

pthread_mutex_t compute_flag_mtx;
pthread_cond_t compute_flag_cond;
extern framebuffer_t GLOBAL_FRAMEBUFFER;
// Synchronization pieces
pthread_mutex_t quit_flag_mtx;
pthread_cond_t quit_signal;
pthread_mutex_t infloop;
pthread_mutex_t tick_mtx;
pthread_mutex_t framebuffer_mtx;
pthread_mutex_t pipe_in_mtx;
pthread_mutex_t pipe_out_mtx;
// Globals
global_data_t global_data = {false, false, NULL, NULL, -1, -1, 200, 200, false};

// Threads
#define THREADS_NUMBER 3
void *event_handler(void *v);
void *pipe_read_thread(void *v);

/* Is called by the pipe in thread to process messages */
/*void act_upon_message(message *msg) {
  event tmp_event;
  switch (msg.type) {
  case MSG_OK:
    debug("got OK");
    break;
  case MSG_ABORT:
    tmp_event = generate_simple_event(EV_KEYBOARD, EV_ABORT);
    queue_push(tmp_event);
    break;
  case EV_SET_COMPUTE:
    if (!set_compute(&msg)) {
      debug("Failed to set compute");
    }
    break;
  case EV_GET_VERSION:
    msg.type = MSG_GET_VERSION;
    break;
  }
}
*/

int main(int argc, char *argv[]) {
  info("Main thread started.");
  pthread_mutex_init(&quit_flag_mtx, NULL);
  pthread_mutex_init(&event_queue_mtx, NULL);
  pthread_cond_init(&exit_signal, NULL);

  global_data.pipe_in_fn = argc > 1 ? argv[1] : "/tmp/computational_module.in";
  global_data.pipe_out_fn =
      argc > 2 ? argv[2] : "/tmp/computational_module.out";

  pthread_t thread_pool[THREADS_NUMBER];
  pthread_create(&thread_pool[0], NULL, keyboard_thread, NULL);
  pthread_create(&thread_pool[1], NULL, compute_thread, NULL);
  pthread_create(&thread_pool[2], NULL, pipe_thread, NULL);

  // Event processing
  event_handler(0);

  for (int i = 0; i < THREADS_NUMBER; i++) {
    pthread_join(thread_pool[i], NULL);
  }

  info("Main thread killed.");
  return 0;
}

// Handles processing of client events
void *event_handler(void *v) {
  pthread_mutex_lock(&quit_flag_mtx);
  bool local_quit = global_data.quit_flag;
  pthread_mutex_unlock(&quit_flag_mtx);
  while (local_quit != true) {
    pthread_mutex_lock(&quit_flag_mtx);
    local_quit = global_data.quit_flag;
    pthread_mutex_unlock(&quit_flag_mtx);

    // THIS BLOCKS THE PROCESS
    event processed_event = queue_pop();
    debug("Received an event");
    switch (processed_event.type) {
    case EV_TYPE_NUM:
    case EV_THREAD_EXIT:
      break;
    case EV_QUIT:
      debug("Received a kill signal!");
      local_quit = kill_program();
      break;
    case EV_GET_VERSION:
      // TODO
      break;
    case EV_COMPUTE:
      break;
    case EV_SET_COMPUTE:
      break;
    default:
      break;
    }
  }
  queue_cleanup();
  info("Event handler died");
  return 0;
}