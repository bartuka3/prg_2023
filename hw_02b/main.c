// Copyright bartuka3 FEE CUT (c) 2023
#include "main.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define SIEVE_SIZE 1000000

int error(int code) {
  switch (code) {
  case INPUT_ERROR:
    fprintf(stderr, "Error: Chybny vstup!\n");
  }
  return code;
}

int digitToInt(char digit) { return digit - '0'; }
char intToDigit(int number) { return number + '0'; }

bool numIsADigit(large_number num, char digit) {
  // Checks if a number happens to be a single digit with `digit`.
  return num.size == 1 && num.value[0] == digit;
}

bool numIsValid(large_number num) {
  // A valid number is longer than 0 chars and isn't zero.
  return num.size >= 1 && !numIsADigit(num, '0');
}

large_number readNum() {
  /* Loads a large_number from stdin. Errors are indicated by negative values of
   * number.size. */
  large_number number;
  number.size = 0;
  number.buf_size = DEFAULT_MALLOC_SIZE;
  number.value = calloc(number.buf_size, sizeof(char));
  if (number.value == NULL) {
    // We failed to calloc
    number.size = _LOAD_ERR_INTERNAL;
    return number;
  }
  // NO I AM OKAY WITH TRUNCATING EOF AS IT STILL ISN'T A NUMBER.
  char buf = getchar(); // we need to preload our buffer
  while (buf != '\0' && buf != '\n' && buf != EOF) {
    // First check if the character happens to be invalid
    if (buf < '0' || buf > '9') {
      free(number.value);
      number.size = _INPUT_ERR_INTERNAL;
      return number;
    }
    // Check if we have enough space to write it + for a null byte, otherwise
    // realloc the value buffer.
    if (number.size >= number.buf_size - 1) {
      number.buf_size = REALLOC_FACTOR(number.buf_size);
      char *tmp = realloc(number.value, number.buf_size * sizeof(char));
      if (tmp == NULL) {
        // Realloc fail
        free(number.value);
        number.size = _LOAD_ERR_INTERNAL;
        return number;
      }
      number.value = tmp;
    }

    // Now we write the current chat and get a new one.
    number.value[number.size] = buf;
    number.size++;
    buf = getchar();
  }
  // Let's be nice and write a null byte.
  number.value[number.size] = '\0';
  return number;
}

large_number divAndReturnRemainder(large_number number, int *mod, int prime) {
  /* Divides the `number` with `prime`, returns the number back,
   * the remainder is written into `*mod`.
   * Speed: O(n) where n is the number of digits in `number`.
   * */
  large_number tmp;
  tmp.size = 0;
  tmp.buf_size = number.buf_size + 1;
  tmp.value = calloc(tmp.buf_size, sizeof(char));
  int index = 0;
  // we load the first digit into our buffer and initialise our modulo
  int buffer = digitToInt(number.value[index]);
  *mod = 0;
  while (buffer < prime && index < number.size) {
    // extracting the first part
    buffer = buffer * 10 + digitToInt(number.value[++index]);
    // but we already have to start counting the modulo
    *mod = buffer % prime;
  }
  while (index < number.size) {
    // We write the computed digit,
    tmp.value[tmp.size++] = intToDigit(buffer / prime);
    // and don't forget to count our modulo
    *mod = buffer % prime;
    // and advance the counter.
    buffer = (buffer % prime) * 10 + digitToInt(number.value[++index]);
  }
  // Don't forget our favourite null byte
  tmp.value[tmp.size + 1] = '\0';
  tmp.buf_size = tmp.size + 1; // Ignore all unused space
  return tmp;
}

void decompose(large_number number, bool sieve[]) {
  // Decomposes a large number using a pre-generated Eratosthenes sieve.
  // Frees `number`, assume `number` is invalid after running this function.

  if (numIsADigit(number, '1')) {
    free(number.value);
    printf("1\n");
    return;
  }

  bool first = true; // don't print the x symbol before the first number
  int modulo_value;  // to store our running modulo
  large_number num_copy = number; // a copy of the number to keep
  large_number tmp;               // a temp number to handle errors etc.

  for (int divisor = 2; divisor <= SIEVE_SIZE && !numIsADigit(num_copy, '1');
       divisor++) {
    // We only continue as long as we haven't reached 1.
    // And only count for actual primes.
    if (!sieve[divisor]) {
      int times_of_division = 0;
      tmp = divAndReturnRemainder(num_copy, &modulo_value, divisor);
      while (!modulo_value) { // same as "The remainder is 0"
        free(num_copy.value); // Wipe out the old value, it's unused now.
        num_copy = tmp;
        times_of_division++;
        tmp = divAndReturnRemainder(num_copy, &modulo_value, divisor);
        // I bet that the Erben's magical cooking pot was just cycled.
        if (numIsADigit(num_copy, '1')) {
          break;
        }
      }

      // Let's be a bit tidy
      free(tmp.value);
      // Now we decide whether the prime is worth keeping or if it gets
      // banished.
      if (times_of_division > 0) {
        // We do not want to print the multiplication sign for the first number
        if (!first) {
          printf(" x ");
        } else {
          first = false;
        }
        if (times_of_division > 1) {
          printf("%d^%d", divisor, times_of_division);
        } else {
          printf("%d", divisor);
        }
      }
    }
  }

  // We looked through all the primes and haven't found all the primes.
  // Regardless, print whatever left.
  if (!numIsADigit(num_copy, '1')) {
    if (!first) {
      printf(" x ");
    }
    printf("%s", num_copy.value);
  }

  printf("\n");
  free(num_copy.value);
}

void generateSieve(bool *sieve) {
  /* Generates an Eratosthenes sieve, where for each number 'false' means prime
   * and 'true' means not prime */
  for (int number_to_test = 2; number_to_test < SIEVE_SIZE; number_to_test++) {
    if (!sieve[number_to_test]) {
      // the first number to be labeled not prime is 2 * unchecked_prime
      int surely_not_prime = number_to_test * 2;
      while (surely_not_prime < SIEVE_SIZE) {
        sieve[surely_not_prime] = true;
        surely_not_prime += number_to_test;
      }
    }
  }
}

int main() {
  static bool sieve[SIEVE_SIZE + 1] = {0}; // is static as it is pre-generated
  generateSieve(sieve);
  large_number input_number;
  // loop stops when zero is entered
  input_number = readNum();

  while (numIsValid(input_number)) {
    printf("Prvociselny rozklad cisla %s je:\n", input_number.value);
    decompose(input_number, sieve);
    input_number = readNum();
  }
  if (input_number.size < 0) { // invalid input or error
    return error(INPUT_ERROR);
  } else {

    free(input_number.value);
    return 0;
  }
}
