// Copyright bartuka3 FEE CUT (c) 2023
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MIN(x, y) ((x) < (y) ? (x) : (y))

#define REALLOC_FACTOR(X) ((X)*2)
const size_t DEFAULT_MALLOC_SIZE = 2;

int error(int code);

typedef struct {
  int size; // also acts as an error state if size < 0
  size_t buf_size;
  char *value;
} large_number;

// We use the internal errors for debugging only, anything < 0 is an error.
enum errors {
  INPUT_ERROR = 100,
  _INPUT_ERR_INTERNAL = -1,
  _LOAD_ERR_INTERNAL = -2
};

void generateSieve(bool *sieve);

large_number readNum();
large_number divAndReturnRemainder(large_number number, int *mod, int prime);
bool numIsADigit(large_number num, char digit);
void decompose(large_number number, bool sieve[]);

int main();
