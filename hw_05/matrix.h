// Copyright bartuka3 FEE CUT (c) 2023
#include "error.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
// math and input errors are flattened, check "error.h" for more info
enum matrix_errors { INPUT_ERROR = 100, MEM_ERROR = 101, MATH_ERROR = 100 };

// Struct borrowed from the coding tutorial
typedef struct {
  int rows;
  int columns;
  int **values;
} matrix;

// PUBLIC INTERFACE
// Matrix manipulation
void printMatrix(matrix printed_matrix);
matrix loadMatrixFromStdIn(int *err);
matrix generateEmptyMatrix(int rows, int columns, int *err);
void destroyMatrix(matrix matrix_to_destroy);

// Matrix mathematics
matrix addTwoMatrices(matrix first, matrix second, int *err);
matrix subTwoMatrices(matrix first, matrix second, int *err);
matrix multiplyTwoMatrices(matrix first, matrix second, int *err);

// I understand that these function names are ridiculously long but adding
// comments to them would be equally ridiculous.
bool matrixAdditionSizeRequirements(matrix first, matrix second);
bool matrixMultiplicationSizeRequirements(matrix first, matrix second);

matrix _addMatrixWithScalarFactor(matrix first, matrix second,
                                  int second_scalar, int *err);
// Internal functions
int _loadIntFromStdIn(int *err);
void *_verifiedCalloc(size_t num_of_members, size_t size, int *err);
