// Copyright bartuka3 FEE CUT (c) 2023
/* ERROR HEADER
 * It might be rather weird to have a separate file with a single function
 * for error messages but the idea is to make the error messages as
 * flexible as possible to comply with whatever exact text the assignment might
 * want while not having to look everywhere in the code just for the function.
 * It also makes debugging considerably easier. That need should hopefully
 * explain the unused error messages.
 *
 * I could have just used the header file alone and define the function in there
 * as well, but it's not a great practice to put code in headers.
 * */

#include <stdio.h>

void error(int code) {
  switch (code) {
  case 100:
    fprintf(stderr, "Error: Chybny vstup!\n");
    break;
  case 101:
    fprintf(stderr, "Memory error\n");
    break;
  case 102:
    fprintf(stderr, "Addition: matrix size error\n");
    break;
  case 103:
    fprintf(stderr, "Multiplication: matrix size error\n");
    break;
  case 104:
    fprintf(stderr, "General math error\n");
    break;
  default:
    fprintf(stderr, "Are you awaiting the Spanish Inquisition?\n");
    break;
  }
}
