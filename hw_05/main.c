// Copyright bartuka3 FEE CUT 2023
#include "matrix.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

enum { plus_sign = '+', minus_sign = '-', mult_sign = '*' };

char readOperatorFromStdIn() {
  // Returns a valid operator symbol read from stdin or '\0'.
  char buff;
  do {
    buff = fgetc(stdin);
    if (buff == plus_sign || buff == minus_sign || buff == mult_sign) {
      return buff;
    }
    // we only continue through whitespace
  } while (buff == ' ' || buff == '\n' || buff == '\t');
  return '\0';
}

int main(int argc, char *argv[]) {
  int ret_value = 0;
  // 1. load the first matrix
  matrix matrix1 = loadMatrixFromStdIn(&ret_value);
  if (ret_value) {
    // We failed to load even the first matrix. To avoid indentation hell we are
    // duplicating code and dying early
    destroyMatrix(matrix1);
    error(ret_value);
    return ret_value;
  }
  // 2. load the operator
  char operator= readOperatorFromStdIn();
  if (operator!= '\0') { // previous function returns an operator or '\0'
    // 3. load the second matrix
    matrix matrix2 = loadMatrixFromStdIn(&ret_value);
    if (!ret_value) { // 2nd matrix load was successful
      // 4. perform the operation
      matrix result;
      if (operator== mult_sign) {
        result = multiplyTwoMatrices(matrix1, matrix2, &ret_value);
      } else if (operator== minus_sign) {
        result = subTwoMatrices(matrix1, matrix2, &ret_value);
      } else {
        // plain addition is the default operation
        result = addTwoMatrices(matrix1, matrix2, &ret_value);
      }
      if (!ret_value) {
        // 5. if everything went smoothly, print the result
        printMatrix(result);
      }
      // 6. delete the temporary result matrix
      destroyMatrix(result);
    }
    // Even if it failed to load, let's clean the second matrix
    destroyMatrix(matrix2);
  } else {
    ret_value = INPUT_ERROR;
  }
  // Destroy the properly loaded first matrix
  destroyMatrix(matrix1);

  if (ret_value) { // Print out any error messages that might have occured
    error(ret_value);
  }
  return ret_value;
}
