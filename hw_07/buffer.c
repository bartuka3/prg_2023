#include <stdio.h>
#include <stdlib.h>
struct node {
    // payload: int
    int payload;
    struct node *next;
};
typedef struct node node_t;

struct linkedList {
    struct node *head;
    struct node *tail;
    size_t size;
};
typedef struct linkedList linked_list_t;

enum errors { MEM_ERROR = 102 };

void error(int code)
{
    if (code == MEM_ERROR) {
        fprintf(stderr, "MEM ERROR!\n");
    }
}

struct linkedList *create_ll()
{
    linked_list_t *ll = malloc(sizeof(struct linkedList));
    if (ll == NULL) {
        error(MEM_ERROR);
    }
    ll->head = NULL;
    ll->tail = NULL;
    ll->size = 0;
    return ll;
}

int ll_push(linked_list_t *_ll, int data)
{
    if (_ll == NULL) {
        return 0;
    }

    node_t *tmp_node = malloc(sizeof(node_t));
    if (tmp_node == NULL) {
        error(MEM_ERROR);
        return 0;
    }
    tmp_node->payload = data;
    tmp_node->next = NULL;
    // EMPTY
    if (_ll->head == NULL) {
        _ll->head = tmp_node;
    }

    if (_ll->tail != NULL) {
        _ll->tail->next = tmp_node;
    }
    _ll->tail = tmp_node;
    _ll->size++;
    return 1;
}

int ll_pop(linked_list_t *_ll)
{
    if (_ll == NULL) {
        return -1;
    }
    if (_ll->head == NULL) {
        return -1;
    }
    node_t *tmp = _ll->head;
    int data = tmp->payload;
    _ll->head = tmp->next;
    if (tmp == _ll->tail) {
        _ll->tail = NULL;
    }
    free(tmp);
    _ll->size--;
    return data;
}

void ll_print(linked_list_t *_ll)
{
    node_t *tmp = _ll->head;
    while (tmp != NULL) {
        printf("%d\n", tmp->payload);
        tmp = tmp->next;
    }
}

void ll_destroy(linked_list_t *_ll)
{
    node_t *tmp = _ll->head;
    while (tmp != NULL) {
        node_t *next_node = tmp->next;
        free(tmp);
        tmp = next_node;
    }
    free(_ll);
}

int main()
{
    linked_list_t *ll = create_ll();
    ll_push(ll, 42);
    ll_push(ll, 44);
    ll_print(ll);
    ll_pop(ll);
    printf("\n-%lu\n", ll->size);
    ll_print(ll);
    ll_pop(ll);
    printf("\n\n");
    ll_print(ll);
        ll_push(ll, 42);
    ll_push(ll, 44);
    ll_destroy(ll);
    return 0;
}
