#include "linked_list.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef NDEBUG
#define NDEBUG
#endif

enum errors { MEM_ERROR = 102 };
#define FAVOURITE_NUMBER 13
struct node {
  int value;
  struct node *prev;
  struct node *next;
};
typedef struct node node_t;
struct linked_list {
  struct node *head;
  struct node *tail;
  size_t size;
};
typedef struct linked_list ll_t;
ll_t LL = {.head = NULL, .tail = NULL, .size = 0};

node_t *create_new_node() {
  node_t *tmp_node = malloc(sizeof(node_t));
  if (tmp_node == NULL) {
    fprintf(stderr, "MEMORY ALLOCATION ERROR!\n");
    exit(101);
  }
  return tmp_node;
}
#ifndef NDEBUG
void ll_print() {
  /*
  node_t *tmp = LL.tail;
    printf("tail - ");
    while (tmp->prev != NULL) {
      printf("%p %d %p - ", tmp->next, tmp->value, tmp->prev);
      tmp = tmp->prev;
    }
    printf("%p %d %p - ", tmp->next, tmp->value, tmp->prev);*/
  printf("\033[36mreversed: ");
  node_t *tmp = LL.head;
  printf("head=%p - ", LL.head);
  while (tmp->next != NULL) {
    printf("%p %d %p - ", tmp->prev, tmp->value, tmp->next);
    tmp = tmp->next;
  }
  printf("%p %d %p - ", tmp->prev, tmp->value, tmp->next);
  printf(" tail=%p with size %lu\033[39m\n", LL.tail, LL.size);
}
#endif

_Bool push(int entry) {
  if (entry < 0) {
    return 0;
  }
  // Create a new son
  node_t *new_node = create_new_node();
  new_node->value = entry;
  new_node->next = NULL;
  new_node->prev = NULL;
  // Check if the list is empty
  if (LL.head == NULL) {
    LL.head = new_node;
    LL.tail = new_node;
  } else {
    new_node->prev = LL.tail;
    // Marry the tail and the new node
    LL.tail->next = new_node;
  }
  // Defile the original tail
  LL.tail = new_node;
  LL.size++;

  return 1;
}

int pop(void) {

  if (LL.tail == NULL || LL.head == NULL) {
    return -FAVOURITE_NUMBER;
  }

  int value = LL.head->value;

  // Turn a peasant into a king
  node_t *new_head = LL.head->next;
  if (new_head != NULL) {
    new_head->prev = NULL;
  }
  // force it to kill its king
  free(LL.head);
  // and defile his grave.
  LL.head = new_head;
  LL.size--;
  return value;
}

/*
 * Insert the given entry to the queue in the InsertSort style.
 *
 * Since push and insert functions can be combined, it cannot be
 * guaranteed, the internal structure of the queue is always sorted.
 *
 * The expected behaviour is that insert proceeds from the head of
 * the queue to the tail in such a way that it is insert before the entry
 * with the lower value, i.e., it becomes a new head if its value is the
 * new maximum or a new tail if its value is a new minimum of the values
 * in the queue.
 *
 * return: true on success; false otherwise
 */
_Bool insert(int entry) {
  if (LL.head == NULL) {
    return push(entry);
  }
  if (LL.head == LL.tail) {
    if (LL.head->value > entry) {
      return push(entry);
    } else {
      node_t *new_first_node = create_new_node();
      new_first_node->value = entry;
      new_first_node->prev = NULL;
      new_first_node->next = LL.head;
      LL.head = new_first_node;
      LL.size++;
      return 1;
    }
  }
  if (LL.head->value <= entry) {
#ifndef NDEBUG
    printf("\033[31mTurning head\033[39m\n");
#endif
    node_t *new_first_node = create_new_node();
    new_first_node->value = entry;
    new_first_node->prev = NULL;
    new_first_node->next = LL.head;
    LL.head->prev = new_first_node;
    LL.head = new_first_node;
    LL.size++;
    return 1;
  }
  // We can assume that the list exists and the LL.head value is bigger than
  // entry.
  node_t *current_node = LL.head;
  node_t *prev_node = LL.head;
  while (current_node != NULL && current_node->value > entry) {
    prev_node = current_node;
    current_node = current_node->next;
  }
  // Now we can insert our new node after prev_node
  if (prev_node == NULL) {
    assert(0);
    return 0;
  }
  node_t *new_node = create_new_node();
  new_node->value = entry;
  new_node->prev = prev_node;
  new_node->next = current_node;

  // rewrite the surrounding elements
  prev_node->next = new_node;
  if (current_node != NULL) {
    current_node->prev = new_node;
  } else {
    LL.tail = new_node;
  }
  LL.size++;
  return 1;
}

/*_Bool insert_olds(int entry) {

  // Unique cases, just push.
  if (LL.head == NULL || LL.head->value < entry) {
#ifndef NDEBUG
    printf("Doing regular push (%d < %d) is true\n", LL.head->value, entry);
#endif
    return push(entry);
  }
  // 1. find a node to append the new one to
  node_t *node_to_append = LL.head;
  node_t *prev_node_to_append = NULL;
  // printf("%d %d \n", node_to_append->value, entry);
  while (node_to_append != NULL && node_to_append->value > entry) {
    // Run until you trip.
    // printf("%d %d \n", node_to_append->value, entry);
    prev_node_to_append = node_to_append;
    node_to_append = node_to_append->next;
  }
  if (node_to_append == NULL) {
    node_to_append = prev_node_to_append;
  }
  // We can now assume there is something before us
  assert(node_to_append != NULL);
  node_t *new_node = create_new_node();
  new_node->prev = node_to_append;
  new_node->value = entry;
  // Adopt the original node's children
  new_node->next = node_to_append->next;

  // CHANGE the node_to_append to link to us
  // CHANGE the node_to_append->next to link back to us, if it exists
  if (node_to_append->next != NULL) {
    node_to_append->next->prev = new_node;
  }
  node_to_append->next = new_node;

  if (node_to_append == LL.tail) {
    LL.tail = new_node;
    new_node->next = NULL;
  }
  LL.size++;
  return 1;
}*/

_Bool erase(int entry) {
  node_t *current_entry = LL.head;
  if (current_entry == NULL) {
    // You are trying to erase void.
    return 0;
  }
  int number_erased = 0;
  while (current_entry != NULL) {
    // Log the next and prev nodes
    node_t *next_entry = current_entry->next;
    node_t *prev_entry = current_entry->prev;

    // We need to cut this element
    if (current_entry->value == entry) {
      number_erased = 1;
      if (current_entry == LL.head) {
        // If we are beheading, we need to attach a new head
        if (next_entry != NULL) {
          next_entry->prev = NULL;
          LL.head = next_entry;
          free(current_entry);
          current_entry = next_entry;
          LL.size--;
          // This element is done, go to the next one
          continue;
        } else {
          // Size 1, we are cutting the entire queue
          LL.head = NULL;
          LL.tail = NULL;
          free(current_entry);
          LL.size--;
          // There's nothing in the queue, we can return
          return 1;
        }
      } else if (current_entry == LL.tail) {
#ifndef NDEBUG
        printf("\033[31mCutting tail\n");
        printf("%p< %d > %p (%p)\033[39m\n", current_entry->prev,
               current_entry->value, current_entry->next, LL.tail);
#endif
        prev_entry->next = NULL;
        LL.tail = prev_entry;
        free(current_entry);
        LL.size--;
        // By definition this is the last element, we can return
        return 1;
      }
      // Here we can already assume both prev_entry and next_entry exist
      prev_entry->next = next_entry;
      next_entry->prev = prev_entry;
      free(current_entry);
      current_entry = next_entry;
      LL.size--;

      continue;
    }
    current_entry = next_entry;
  }
#ifndef NDEBUG
  printf("\033[31mDone cutting\033[39m\n");
  ll_print();
#endif
  return number_erased;
}

int getEntry(int idx) {
  node_t *current_entry = LL.head;
  while (current_entry != NULL && idx > 0) {
    // printf(" %d %p", current_entry->value, current_entry->next);
    current_entry = current_entry->next;
    idx--;
  }
  if (current_entry == NULL) {
    return 0;
  }
  return current_entry->value;
}

int size(void) { return LL.size; }

void clear() {
  if (LL.head == NULL) {
    return;
  }
  node_t *tmp_node = LL.head;
  while (tmp_node != NULL) {
    node_t *next_node = tmp_node->next;
    free(tmp_node);
    tmp_node = next_node;
    LL.size--;
  }
  assert(!LL.size);
  LL.head = NULL;
  LL.tail = NULL;
}
