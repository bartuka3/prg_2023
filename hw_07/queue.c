#include <stdio.h>
#include <stdlib.h>
struct node {
    // payload: int
    int payload;
    int priority;
    struct node *next;
    struct node *prev;
};
typedef struct node node_t;

struct linkedList {
    struct node *head;
    struct node *tail;
    size_t size;
};
typedef struct linkedList linked_list_t;

enum errors { MEM_ERROR = 102 };

void error(int code)
{
    if (code == MEM_ERROR) {
        fprintf(stderr, "MEM ERROR!\n");
    }
}

struct linkedList *create_ll()
{
    linked_list_t *ll = malloc(sizeof(struct linkedList));
    if (ll == NULL) {
        error(MEM_ERROR);
    }
    ll->head = NULL;
    ll->tail = NULL;
    ll->size = 0;
    return ll;
}

int ll_push(linked_list_t *_ll, int data, int priority)
{
    if (_ll == NULL) {
        return 0;
    }

    node_t *tmp_node = malloc(sizeof(node_t));
    if (tmp_node == NULL) {
        error(MEM_ERROR);
        return 0;
    }
    tmp_node->payload = data;
    tmp_node->next = NULL;
    tmp_node->prev = NULL;
    tmp_node->priority = priority;
    // EMPTY
    if (_ll->head == NULL) {
        _ll->head = tmp_node;
    }

    if (_ll->tail != NULL) {

        _ll->tail->next = tmp_node;
    }
    tmp_node->prev = _ll->tail;
    _ll->tail = tmp_node;
    _ll->size++;
    return 1;
}

int ll_pop(linked_list_t *_ll)
{
    if (_ll == NULL) {
        return -1;
    }
    if (_ll->tail == NULL) {
        return -1;
    }
    node_t *tmp = _ll->tail;
    int data = tmp->payload;
    _ll->tail = tmp->prev;
    if (tmp == _ll->tail) {
        _ll->tail = NULL;
    }
    free(tmp);
    _ll->size--;
    return data;
}

void ll_print(linked_list_t *_ll)
{
    node_t *tmp = _ll->tail;
    while (tmp != NULL) {
        printf("%d %d\n", tmp->payload, tmp->priority);
        tmp = tmp->prev;
    }
}

void ll_destroy(linked_list_t *_ll)
{
    node_t *tmp = _ll->tail;
    while (tmp != NULL) {
        node_t *next_node = tmp->prev;
        free(tmp);
        tmp = next_node;
    }
    free(_ll);
}

int main()
{
    linked_list_t *ll = create_ll();
    ll_push(ll, 42, 1);
    ll_push(ll, 44, 1);
    ll_push(ll, 48, 0);
    ll_push(ll, 49, 0);
    ll_print(ll);
    ll_pop(ll);
    printf("\n::%lu\n", ll->size);
    ll_print(ll);
    ll_pop(ll);
    printf("\n::%lu\n", ll->size);
    ll_print(ll);
    ll_push(ll, 42, 0);
    ll_push(ll, 44, 0);

    ll_destroy(ll);
    return 0;
}
