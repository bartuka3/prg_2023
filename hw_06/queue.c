/*
 * Copyright (c) 2023, Karel Bartůněk, FEE CTU <bartuka3@fel.cvut.cz>
 */

#include "queue.h"
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#ifndef NDEBUG
// Comment out the next line to ENABLE DEBUG output
#define NDEBUG
#endif

void error(int code) {
  switch (code) {
  case MEM_ERROR:
    fprintf(stderr, "Memory error!\n");
    break;
  case INPUT_ERROR:
    fprintf(stderr, "Wrong input!\n");
    break;
  default:
    fprintf(stderr, "I AM ERROR\n");
    break;
  }
}
/* Debug print of the entire queue to stderr. */
void __print_queue(queue_t *q) {
#ifndef NDEBUG
  fprintf(stderr, "\033[104mDEBUG:\033[49m Q: %d:%d, size %d, allocated %d\n",
          q->start, q->end, q->size, q->allocated_size);
  if (q->size) {
    fprintf(stderr, "VALUES:[ ");
    int j = 0;
    for (int i = q->start; i != q->end; i = get_index(q, j)) {
      fprintf(stderr, "%p, ", q->m_queue[i]);
      j++;
    }
    fprintf(stderr, "]\n");
  }
#endif
}
// for size:=X we need to trigger at end:= X - 1 already.
bool is_full(queue_t *q) { return (q->end >= q->allocated_size - 1); }

bool is_empty(queue_t *q) { return q->size; }

/* A general purpose queue resizer, has O(n) complexity as it copies the data
 * manually per-element.
 * Returns true on success, false on fail
 * */
bool _resize_queue(queue_t *queue, int new_capacity) {
  // A special case for when destroying the queue
  if (!(new_capacity)) {
    free(queue->m_queue);
    queue->allocated_size = new_capacity;
    return true;
  }
  // We first allocate a completely new queue array
  void **tmp_queue = malloc(sizeof(void *) * new_capacity);
  if (tmp_queue == NULL) {
    return false;
  }
  // A special case for initialising the queue, we do not need to copy anything
  if (queue->m_queue == NULL) {
    queue->m_queue = tmp_queue;
    queue->allocated_size = new_capacity;
    return true;
  }
  assert(new_capacity >= queue->size); // We can't be cropping data

  // Copy the queue with O(n) time complexity and set the appropriate indexes.
  int j = 0;
  for (int i = queue->start; i != queue->end; i = get_index(queue, j)) {
    tmp_queue[j++] = queue->m_queue[i];
  }
  queue->start = 0;
  queue->end = queue->size;
  // Now we can safely destroy the old queue and replace it
  free(queue->m_queue);
  queue->m_queue = tmp_queue;
  // Finally, we can proclaim the new allocated size to be true.
  queue->allocated_size = new_capacity;
  return true;
}

/* creates a new queue with a given size */
queue_t *create_queue(int capacity) {
  queue_t *queue = malloc(sizeof(queue_t));
  if (queue == NULL) {
    // ERROR
    error(MEM_ERROR);
    return queue;
  }
  queue->size = 0;
  queue->start = 0;
  queue->end = 0;
  queue->m_queue = NULL;

  if (!(_resize_queue(queue, capacity))) {
    // ERROR
    error(MEM_ERROR);
    return queue;
  }

  return queue;
}

/* deletes the queue and all allocated memory */
void delete_queue(queue_t *queue) {
  assert(!is_empty(queue)); // Please do not hurt me :'(
  // There is no need to check the memory safety now.
  _resize_queue(queue, 0);
  free(queue);
}

/* Checks whether we need to make the internal array bigger and does so if it's
 * needed.
 * Returns true on success, false on error. */
bool test_and_increase_size(queue_t *queue) {
#ifndef NDEBUG
  fprintf(stderr, "\033[104mDEBUG:\033[49m PUSH\n");
  __print_queue(queue);
#endif
  if (is_full(queue)) {
    if (!(_resize_queue(queue, queue->allocated_size * INCREASE_FACTOR))) {
      return false;
    }
  }
  return true;
}

/* Checks whether we can to reduce the internal array and does so if it's
 * needed.
 * Returns true on success, false on error. */
bool test_and_decrease_size(queue_t *queue) {
#ifndef NDEBUG
  fprintf(stderr, "\033[104mDEBUG:\033[49m POP\n");
  __print_queue(queue);
#endif
  if (queue->size * DECREASE_FACTOR <= queue->allocated_size) {
    if (!(_resize_queue(queue, queue->allocated_size / DECREASE_FACTOR + 1))) {
      return false;
    }
  }
  return true;
}

/* Converts a queue index to an array index  */
int get_index(queue_t *queue, int idx) {
  return (queue->start + idx) % queue->allocated_size;
}

/*
 * inserts a reference to the element into the queue
 * returns: true on success; false otherwise
 */
int push_to_queue(queue_t *queue, void *data) {
  if (!test_and_increase_size(queue)) {
    error(MEM_ERROR);
    return 0;
  }
  // Here we can already assume the queue is big enough
  queue->m_queue[queue->end] = data;
  // Move the end position
  queue->end = get_index(queue, queue->size + 1);
  queue->size++;
  return 1;
}

/*
 * gets the first element from the queue and removes it from the queue
 * returns: the first element on success; NULL otherwise
 */
void *pop_from_queue(queue_t *queue) {
  // We cannot pop from an empty queue
  if (queue->size == 0) {
    return NULL;
  }
  if (!test_and_decrease_size(queue)) {
    // ERROR STATE
    error(MEM_ERROR);
    return NULL;
  }
  // We take the value from the start
  void *value = queue->m_queue[queue->start];
  // We shift the start by one
  queue->start = get_index(queue, 1);
  queue->size--;
  return value;
}

/*
 * Gets idx-th element from the queue.
 * Retrieves the element that would be popped after idx calls of the
 * pop_from_queue().
 * Returns: the idx-th element on success; NULL otherwise
 */
void *get_from_queue(queue_t *queue, int idx) {
  // Check that we aren't trying to access unallocated memory / nonsense
  if (idx >= queue->size || idx < 0) {
    return NULL;
  }
  // Return the idx-th element from the queue
  return queue->m_queue[get_index(queue, idx)];
}

/* Gets the number of stored elements */
int get_queue_size(queue_t *queue) { return queue->size; }
