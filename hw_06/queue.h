/*
 * Copyright (c) 2023, Karel Bartůněk, FEE CTU <bartuka3@fel.cvut.cz>
 * Function definitions from the assignment were used.
 */
#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Error states */
enum errors { MEM_ERROR = 102, INPUT_ERROR = 100 };
void error(int code);
/* Constants for dynamic reallocation */
#define INCREASE_FACTOR 2
#define DECREASE_FACTOR 3

/* Queue structure which holds all necessary data */
typedef struct {
  int start;
  int end;
  int size;
  int allocated_size;
  void **m_queue;
} queue_t;

/* A general purpose (re)sizer for m_queue */
bool _resize_queue(queue_t *queue, int new_capacity);

/* Size evaluators */
bool is_full(queue_t *q);
bool is_empty(queue_t *q);

/*
 * Queue resizers with testing, to be used in public functions
 * return: true on error, false otherwise
 * */
bool test_and_increase_size(queue_t *q);
bool test_and_decrease_size(queue_t *q);

/* Converts a queue index to an array index  */
int get_index(queue_t *queue, int idx);

/* creates a new queue with a given size */
queue_t *create_queue(int capacity);
/* deletes the queue and all allocated memory */
void delete_queue(queue_t *queue);

/*
 * inserts a reference to the element into the queue
 * returns: true on success; false otherwise
 */
int push_to_queue(queue_t *queue, void *data);

/*
 * gets the first element from the queue and removes it from the queue
 * returns: the first element on success; NULL otherwise
 */
void *pop_from_queue(queue_t *queue);

/*
 * gets idx-th element from the queue
 * returns the element that would be popped after idx calls of the
 * pop_from_queue() returns: the idx-th element on success; NULL otherwise
 */
void *get_from_queue(queue_t *queue, int idx);

/* gets number of stored elements */
int get_queue_size(queue_t *queue);

#endif /* __QUEUE_H__ */
