// Copyright (c) Karel Bartůněk FEE CTU <bartuka3@fel.cvut.cz> 2023

#include "main.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

int error(int code) {
  switch (code) {
  case 100:
    fprintf(stderr, "Error: Chybny vstup!\n");
    return 100;
  case 101:
    fprintf(stderr, "Error: Chybna delka vstupu!\n");
    return 101;
  }
  return code;
}

char *loadString() {
  size_t str_size = DEFAULT_SIZE;
  char *string = (char *)malloc(str_size);
  char buffer = '\0';  // writing the current character here
  size_t position = 0; // current position in string to write to
  do {
    buffer = getchar();
    if (buffer == '\n') { // do not forget to add a null byte
      buffer = '\0';
    }
    if (position * sizeof(char) >= str_size - 1) { // we need to realloc
      str_size = EXPAND_FACTOR(str_size); // dangerous, thus we error on fail
      char *tmp = (char *)realloc(string, str_size);
      if (!tmp) { // failed load
        free(string);
        string = NULL;
        return (char *)'\0';
      }
      string = tmp; // pointer swapping
    }
    string[position++] = buffer;
  } while (buffer);
  return string;
}

int compare(char *str, char *str2, size_t size) {
  int similarity = 0;
  for (size_t i = 0; i < size; i++) {
    if (str[i] == str2[i]) {
      similarity++;
    }
  }
  return similarity;
}

char rotate(char original, int offset) {
  for (int i = 0; i < offset; i++) {
    if (original == 'Z') { // Z -> a
      original = 'a';
    } else if (original == 'z') { // z -> a
      original = 'A';
    } else {
      original++;
    }
  }
  return original;
}

void shift(char *src, char *dst, size_t size, int offset) {
  for (int j = 0; j < size; j++) {
    dst[j] = rotate(src[j], offset);
  }
}

size_t getStrLenAndVerify(char *str) {
  /* O(n) complexity */
  size_t index = 0;
  while ((char)str[index] != '\0') {
        if (!(VALID_CHAR(str[index])) {
      return -1; // INVALID CHARACTER FOUND
        }
        index++;
  }
  return index;
}

void printStr(char *string) {
  /* O(n) complexity printing */
  size_t i = 0;
  while (string[i] != '\0') {
    printf("%c", string[i]); // putchar() would lead to undefined behaviour here
    i++;
  }
  putchar('\n'); // potentially faster than printf('\n')
}

int minimum(int a, int b, int c) {
  if (a > b) {
    return b < c ? b : c;
  } else {
    return a < c ? a : c;
  }
}
void printMatrix(int **matrix, size_t size1, size_t size2) {
  for (int i = 0; i <= size1; i++) {
    for (int j = 0; j <= size2; j++) {
      printf(" %d ", matrix[i][j]);
    }
    printf("\n");
  }
  return;
  for (int i = size1 > 2 ? size1 - 2 : 0; i <= size1; i++) {
    for (int j = size2 > 2 ? size2 - 2 : 0; j <= size2; j++) {
      printf(" %d ", matrix[i][j]);
    }
    printf("\n");
  }
}

int WagnerFischer(char *str1, char *str2, size_t size1, size_t size2) {
  // https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm
  // We can also safely reuse the distance matrix, so we do. We initialize it as
  // a global variable.
  int result = size1 * size2;

  distance_matrix = calloc(size1 + 2, sizeof(int *));
  if (distance_matrix == NULL) { // check the first layer load
    return result;
  }
  for (int x = 0; x <= size1; x++) {
    distance_matrix[x] = calloc(size2 + 2, sizeof(int));
    if (distance_matrix[x] == NULL) { // a partial array load failed, we need to
                                      // remove the remaining lines
      for (int nx = 0; nx < x; nx++) {
        free(distance_matrix[x]);
      }
      return result;
    }
  }

  for (int i = 1; i <= size1; i++) {
    distance_matrix[i][0] = i;
  }
  for (int j = 1; j <= size2; j++) {
    distance_matrix[0][j] = j;
  }
  int sub_cost;
  for (int j = 1; j <= size2; j++) {
    for (int i = 1; i <= size1; i++) {
      sub_cost = str1[i - 1] == str2[j - 1] ? 0 : 1;
      distance_matrix[i][j] =
          minimum(distance_matrix[i - 1][j] + 1, distance_matrix[i][j - 1] + 1,
                  distance_matrix[i - 1][i - 1] + sub_cost);
      if (distance_matrix[i][j] == 0 && (i > 2) && (j > 2)) {
        printf("ERROOOOOOOOOR %d %d %d - %d %d\n", distance_matrix[i - 1][j],
               distance_matrix[i][j - 1],
               distance_matrix[i - 1][i - 1] + sub_cost, i, j);
        printMatrix(distance_matrix, size1, size2);
        assert(false);
      }
    }
  }
  result = distance_matrix[size1][size2];

  // printMatrix(distance_matrix, size1, size2);

  // Free the distance matrix
  for (int x = 0; x <= size1; x++) {
    free(distance_matrix[x]);
  }
  free(distance_matrix);

  return result;
}

int main() {
  char *ciphertext = loadString();
  char *untrusted_record = loadString();
  size_t cipher_size = getStrLenAndVerify(ciphertext);
  size_t record_size = getStrLenAndVerify(untrusted_record);

  int status_code = 0;
  if (cipher_size == -1 || record_size == -1) { // invalid strings
    status_code = error(100);
    goto free_and_exit;
  }
  /*
  if (cipher_size != record_size) { // inconsistent size
    status_code = error(101);
    goto free_and_exit;
  }*/

  // Allocate the distance matrix

  int closest_shift = 0; // best recorded match
  int closest =
      cipher_size * record_size; // the closeness of the best recorded match
  char *shifted_text = (char *)malloc(cipher_size + sizeof(char));
  shifted_text[cipher_size / sizeof(char)] = '\0'; // set last char to 0
  /* ^^^ could replace the div with bitshift but sizeof(char) isn't set in stone
   */
  for (int shift_iter = 0; shift_iter <= CHAR_NUM; shift_iter++) {
    shift(ciphertext, shifted_text, cipher_size, shift_iter);

    printf("%u %u \n", (unsigned int)cipher_size, (unsigned int)record_size);

    int distance =
        WagnerFischer(untrusted_record, shifted_text, record_size, cipher_size);
    //    int distance = compare(untrusted_record, shifted_text, cipher_size);
    // We need to check if we are actually closer to the untrusted record
    if (distance < closest) {
      printf("\033[36mhit\033[39m\n\n");
      closest_shift = shift_iter;
      closest = distance;

      printMatrix(distance_matrix, record_size, cipher_size);
    }
    printf("%d shift %d distance %d closest [%d]  \n", shift_iter, distance,
           closest, closest_shift);
    printf("%s\n%s\n", shifted_text, untrusted_record);
  }

  // Since we now know what is the closest to the ciphertext, we push it to
  // shifted_text
  shift(ciphertext, shifted_text, cipher_size, closest_shift);

  printStr(shifted_text);

  // Now we can free destination string
  free(shifted_text);

// At this point we can free all the memory
free_and_exit:
  free(ciphertext);
  free(untrusted_record);

  return status_code;
}
