// Copyright bartuka3 FEE CUT (c) 2023
#include <stdbool.h>

int error(int code);
long readNum(void);

/*
int nmod(long a, int b);
int ndiv(long a, int b);
int sqrt(number);
*/

void decompose(long number, bool sieve[]);
void generateSieve(bool *sieve);

int main();
