// Copyright bartuka3 FEE CUT (c) 2023
#include "main.h"
#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#define SIEVE_SIZE 1000000
#define MIN(x, y) ((x) < (y) ? (x) : (y))

/* While the performance impact of using separate division and modulo functions
 * instead of macros for small numbers is insignificant (about 26 lines of x86
 * assembly in -O3 for me), I have decided to make them macros for the mandatory
 * task anyway. */
#define DIV(x, y) ((x) / (y)) // Future-proofing for big numbers
#define MOD(x, y) ((x) % (y)) // Future-proofing for big numbers

int error(int code) {
  switch (code) {
  case 100:
    fprintf(stderr, "Error: Chybny vstup!\n");
  }
  return code;
}

long readNum(void) {
  long number = -1;
  if (scanf("%ld", &number) != 1) {
    return -1;
  }
  return number;
}

/*int nmod(long a, int b) {
    // separated for implementation for large numbers
    // returns a modulo of two numbers
    return a % b;
}

int ndiv(long a, int b) {
    // separated for implementation for large numbers
    // returns a division of two numbers
    return a / b;

int sqrt(number) {
    // in the future sqrt(num) will always be bigger than 10^6, so not bothering
    // with the square root of a great number might be useful
    return number
 }
}*/

void decompose(long number, bool sieve[]) {
  /* Decomposes a 'number' using a pre-generated Eratosthenes 'sieve'. */
  // exception for 1: 1 always decomposes to 1
  if (number == 1) {
    printf("1\n");
    return;
  }
  bool first = true; // we don't print the x symbol before the first number
  for (int divisor = 2; divisor <= MIN(SIEVE_SIZE, sqrt(number)); divisor++) {
    // since at least number > 1, we can start at 2

    if (!sieve[divisor]) {
      int times_of_division = 0; // times of division
      while (!MOD(number, divisor)) {
        times_of_division += 1;
        number = DIV(number, divisor);
      }
      if (times_of_division) {
        if (!first) {
          printf(" x "); // multiplication sign
        }
        if (times_of_division == 1) {
          printf("%d", divisor); // we only divided the number one time
        } else {
          printf("%d^%d", divisor, times_of_division);
        }
        first = false; // we can start printing multiplication signs now
      }
    }
  }

  if (number > 1) {
    // protection in case we leave early / do not catch something
    if (!first) {
      printf(" x "); // multiplication sign
    }
    printf("%ld", number);
  }
  printf("\n");
}

void generateSieve(bool *sieve) {
  /* Generates an Eratosthenes sieve, where for each number 'false' means prime
   * and 'true' means not prime */
  for (int number_to_test = 2; number_to_test < SIEVE_SIZE; number_to_test++) {
    if (!sieve[number_to_test]) {
      // the first number to be labeled not prime is 2 * unchecked_prime
      int surely_not_prime = number_to_test * 2;
      while (surely_not_prime < SIEVE_SIZE) {
        sieve[surely_not_prime] = true;
        surely_not_prime += number_to_test;
      }
    }
  }
}

int main() {
  static bool sieve[SIEVE_SIZE + 1] = {0}; // is static as it is pre-generated
  generateSieve(sieve);
  long input_number;
  while ((input_number = readNum()) > 0) { // loop stops when zero is entered
    printf("Prvociselny rozklad cisla %ld je:\n", input_number);
    decompose(input_number, sieve);
  }

  if (input_number < 0) { // negative number entered
    return error(100);
  }

  return 0;
}
