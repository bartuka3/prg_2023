/* © bartuka3, FEE CUT 2023 */
#include "main.h"
#include <stdio.h>
#include <stdlib.h>

int error(int code) {
  switch (code) {
  case 100:
    fprintf(stderr, "Error: Chybny vstup!\n");
    return 100;
  case 101:
    fprintf(stderr, "Error: Chybna delka vstupu!\n");
    return 101;
  }
  return code;
}

char *loadString() {
  size_t str_size = DEFAULT_SIZE;
  char *string = (char *)malloc(str_size);
  char buffer = '\0';  // writing the current character here
  size_t position = 0; // current position in string to write to
  do {
    buffer = getchar();
    if (buffer == '\n') { // do not forget to add a null byte
      buffer = '\0';
    }
    if (position * sizeof(char) >= str_size - 1) { // we need to realloc
      str_size = EXPAND_FACTOR(str_size); // dangerous, thus we error on fail
      char *tmp = (char *)realloc(string, str_size);
      if (!tmp) { // failed load
        free(string);
        string = NULL;
        return (char *)'\0';
      }
      string = tmp; // pointer swapping
    }
    string[position++] = buffer;
  } while (buffer);
  return string;
}

int compare(char *str, char *str2, size_t size) {
  int similarity = 0;
  for (size_t i = 0; i < size; i++) {
    if (str[i] == str2[i]) {
      similarity++;
    }
  }
  return similarity;
}

char rotate(char original, int offset) {
  for (int i = 0; i < offset; i++) {
    if (original == 'Z') { // Z -> a
      original = 'a';
    } else if (original == 'z') { // z -> a
      original = 'A';
    } else {
      original++;
    }
  }
  return original;
}

void shift(char *src, char *dst, size_t size, int offset) {
  for (int j = 0; j < size; j++) {
    dst[j] = rotate(src[j], offset);
  }
}

size_t getStrLenAndVerify(char *str) {
  /* O(n) complexity */
  size_t index = 0;
  while ((char)str[index] != '\0') {
        if (!(VALID_CHAR(str[index])) {
      return -1; // INVALID CHARACTER FOUND
        }
        index++;
  }
  return index;
}

void printStr(char *string) {
  /* O(n) complexity printing */
  size_t i = 0;
  while (string[i] != '\0') {
    printf("%c", string[i]); // putchar() would lead to undefined behaviour here
    i++;
  }
  putchar('\n'); // potentially faster than printf('\n')
}

int main() {
  char *ciphertext = loadString();
  char *untrusted_record = loadString();
  size_t cipher_size = getStrLenAndVerify(ciphertext);
  size_t record_size = getStrLenAndVerify(untrusted_record);

  int status_code = 0;
  if (cipher_size == -1 || record_size == -1) { // invalid strings
    status_code = error(100);
    goto free_and_exit;
  }
  if (cipher_size != record_size) { // inconsistent size
    status_code = error(101);
    goto free_and_exit;
  }

  int closest_shift = 0; // best recorded match
  int closest = 0;       // the closeness of the best recorded match
  char *shifted_text = (char *)malloc(cipher_size + sizeof(char));
  shifted_text[cipher_size / sizeof(char)] = '\0'; // set last char to 0
  /* ^^^ could replace the div with bitshift but sizeof(char) isn't set in stone
   */
  for (int shift_iter = 0; shift_iter <= CHAR_NUM; shift_iter++) {
    shift(ciphertext, shifted_text, cipher_size, shift_iter);
    int closeness = compare(untrusted_record, shifted_text, cipher_size);
    // We need to check if we are actually closer to the untrusted record
    if (closeness > closest) {
      closest_shift = shift_iter;
      closest = closeness;
    }
  }

  // Since we now know what is the closest to the ciphertext, we push it to
  // shifted_text
  shift(ciphertext, shifted_text, cipher_size, closest_shift);

  printStr(shifted_text);

  // Now we can free destination string
  free(shifted_text);

// At this point we can free all the memory
free_and_exit:
  free(ciphertext);
  free(untrusted_record);

  return status_code;
}
