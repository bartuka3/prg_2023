/* © bartuka3, FEE CUT 2023 */
#include <stddef.h>

#define DEFAULT_SIZE 100 * sizeof(char)
#define EXPAND_FACTOR(X) (2 * X + sizeof(char)) // 2 * size_str + 1 for \0
#define VALID_CHAR(CH) ((CH >= 'A' && CH <= 'Z') || (CH >= 'a' && CH <= 'z')))
#define CHAR_NUM 52

char *loadString();                 // loads a string dynamically
size_t getStrLenIfValid(char *str); // validates a string and returns its length
void printStr(char *string);        // prints a string by characters

int compare(char *str, char *str2, size_t size); // compares two strings
void shift(char *src, char *dst, size_t size,
           int offset); // shifts src by offset and pushes it to dst

int main();
